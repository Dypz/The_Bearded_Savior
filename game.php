<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<title>RPG Phaser</title>
		<link href='http://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="scripts/js/phaser.js"></script>
		<script type="text/javascript" src="scripts/js/NPC.js"></script>
		<script type="text/javascript" src="scripts/js/Dialogue.js"></script>
		<script type="text/javascript" src="scripts/js/Ability.js"></script>
		<script type="text/javascript" src="scripts/js/Character.js"></script>
		<script type="text/javascript" src="scripts/js/Monster.js"></script>
		<script type="text/javascript" src="scripts/js/Boot.js"></script>
		<script type="text/javascript" src="scripts/js/Preloader.js"></script>
		<script type="text/javascript" src="scripts/js/MainMenu.js"></script>
		<script type="text/javascript" src="scripts/js/CharacterSelect.js"></script>
		<script type="text/javascript" src="scripts/js/CharacterCreation.js"></script>
		<script type="text/javascript" src="scripts/js/Game.js"></script>
		<script type="text/javascript" src="scripts/js/Intro.js"></script>
		<script type="text/javascript" src="scripts/js/Boss.js"></script>
		<script type="text/javascript" src="http://localhost:8080/socket.io/socket.io.js"></script>
		<script type="text/javascript" src="scripts/js/jquery.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var game = new Phaser.Game(800, 600, Phaser.AUTO, 'game');

				WebFontConfig = {
				    active: function() { game.time.events.add(Phaser.Timer.SECOND, function() {}, this); },

				    google: {
				      families: ['Roboto Slab']
				    }

				};

				game.state.add('Boot', RPG.Boot);
				game.state.add('Preloader', RPG.Preloader);
				game.state.add('MainMenu', RPG.MainMenu);
				game.state.add('CharacterSelect', RPG.CharacterSelect);
				game.state.add('CharacterCreation', RPG.CharacterCreation);
				game.state.add('Game',RPG.Game);
				game.state.add('Intro', RPG.Intro);
				game.state.start('Boot');
			});	
		</script>
	</head>
	<body>
		<?php 
			session_start();
			if(!(isset($_SESSION['username'])) || !(isset($_SESSION['password']))) // si une des sessions n'existe pas, on renvoie l'utilisateur sur la page d'accueil
			{
				$_SESSION['informations'] = 'Rejected because you didn\'t authenficate ';
				header('location: index.php');
			}
			else // nos 2 sessions existent, on va vérifier qu'elles correspondent avec celles de la BDD
			{
				require './includes_php/connect.php';
		
				$back = $db->query("SELECT username, password FROM users WHERE username = \"".$_SESSION['username']."\"");	
				$row = $back->fetch_array();
				if($row['username'] != '')
				{
					if($row['password'] == $_SESSION['password'])
					{
						// dans ce cas tout est valide, on confirme les sessions
						$_SESSION['username'] = $row['username'];
						$_SESSION['password'] = $row['password'];
					}
					else
					{
						$_SESSION['informations'] = 'Rejected because of a bad session password ' ;
						unset($_SESSION['username']);
						unset($_SESSION['password']);
						header('location: index.php');
					}
				}
				else
				{
					$_SESSION['informations'] = 'Rejected because your username does\'t exist ' ;
					unset($_SESSION['username']);
					unset($_SESSION['password']);
					header('location: index.php');
				}
			}

			if (!empty($_SESSION['username'])) {
				$username = $_SESSION['username']; // on récupère la session avec le username
			}
		?>
			<script type="text/javascript">
				var username = sessionStorage.getItem("username"); 
				if(username == null) // si user_name n'a pas encore été stocké dans une sessionStorage alors on le récupère par le php
				{
					username = '<?php echo $username ?>';// on stocke dans une sessionStorage la valeur récupérée par le php
					sessionStorage.setItem("username",username); // on crée une sessionStorage 
				}
			</script>
		<div id="game"></div>
	</body>
</html>