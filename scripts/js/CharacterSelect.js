RPG.CharacterSelect = function(game) {
	RPG.socket = io.connect('http://localhost:8080');
	RPG.char_object_list = new Array();
	RPG.selected;
	RPG.nb_char_find = 0;
	RPG.char_tab = new Array();
	RPG.sprite = new Array();
	RPG.opacity_sprite = 0.6;
	RPG.opacity_text = 0.6;
	RPG.can_move = true;
};

RPG.CharacterSelect.prototype = {
	preload: function() {
		this.background = this.add.tileSprite(0, 0, 800, 600, 'CSBackground');
	},

	create: function() {
		this.createText();
	},

	createCharAction: function() {
		if(RPG.nb_char_find < 3)
		{	
			RPG.selected = null;
			this.state.start('CharacterCreation');
		}
		else
		{
			alert('Delete a character if you want to create another ( 3 characters max per account )');
		}
	},

	deleteCharAction: function() {
		if(typeof(RPG.selected) != 'undefined')
		{
			RPG.socket.emit('delete_character',JSON.stringify(RPG.selected));

			RPG.socket.on('delete_succesfull',function(){
				document.location.reload(true);
			});
		}
		else
		{
			alert('If you want to delete a character, you must select one of them');
		}
	},

	loadCharAction: function() {
		if(typeof(RPG.selected) != 'undefined')
		{
			this.state.start('Game');
		}
		else
		{	
			alert('You must choose a character to play or create an other');
		}
	},

	createListCharacter : function(argue){
		var textStyle;

		if(argue == 'None')
		{
			textStyle = {
			font: "20px Lucida Console",
			fill: "white",
			align: "center"
			};
			this.add.text(this.world.centerX-200,500,"No character saved for this account",textStyle);
		}
		else
		{
			textStyle = {
				font: "20px Lucida Console",
				fill: "white",
				align: "center"
			};
			this.char_tab = new Array();
			for(var i=0;i<argue.length;i++)
			{
				switch(i){
					case 0: 
						RPG.char_tab[i] = this.add.text(360,440,argue[i].level +' : '+argue[i].name,textStyle);
						break;
					case 1:
						RPG.char_tab[i] = this.add.text(575,400,argue[i].level +' : '+argue[i].name,textStyle);
						RPG.char_tab[i].alpha = RPG.opacity_text;
						break;
					case 2:
						RPG.char_tab[i] = this.add.text(145,400,argue[i].level +	' : '+argue[i].name,textStyle);
						RPG.char_tab[i].alpha = RPG.opacity_text;
						break;
				};
				switch(argue[i].job){
					case 'Neogician' :
						switch(i)
						{
							case 0:  
								RPG.sprite[i] = this.add.sprite(300,210,'neogician');
								RPG.sprite[i].scale.setTo(0.8);
								break;
							case 1:
								RPG.sprite[i] = this.add.sprite(575,280,'neogician');
								RPG.sprite[i].scale.setTo(0.4);
								RPG.sprite[i].alpha = RPG.opacity_sprite;
								break;
							case 2:
								RPG.sprite[i] = this.add.sprite(125,280,'neogician');
								RPG.sprite[i].scale.setTo(0.4);
								RPG.sprite[i].alpha = RPG.opacity_sprite;
								break;
						};
						break;
					case 'Berseker' :
						switch(i)
						{
							case 0:
								RPG.sprite[i] = this.add.sprite(300,210,'berseker');
								RPG.sprite[i].scale.setTo(0.8);
								break;
							case 1:
								RPG.sprite[i] = this.add.sprite(575,280,'berseker');
								RPG.sprite[i].scale.setTo(0.4);
								RPG.sprite[i].alpha = RPG.opacity_sprite;
								break;
							case 2:
								RPG.sprite[i] = this.add.sprite(125,280,'berseker');
								RPG.sprite[i].scale.setTo(0.4);
								RPG.sprite[i].alpha = RPG.opacity_sprite;
								break;
						};
						break;
				};
			}
			RPG.selected = RPG.char_object_list[0]; // de base le personnage sélectionné est celui à l'indice 0
		}
	},

	swap : function(tab,i,j,k){ // fonction pour swap 2 éléments dans un tableau aux indices i et j
		if(k == -1)				// ou avec 3 éléments : i -> j, j -> k, k -> i (-> = élément à l'indice i passe à l'indice j)
		{
			var tmp = tab[i];
			tab[i] = tab[j];
			tab[j] = tmp;
		}
		else
		{
			var tmp1 = tab[i];
			var tmp2 = tab[j];
			tab[i] = tab[k];
			tab[j] = tmp1;
			tab[k] = tmp2;
		}
	},

	swapRight : function(){
		if(RPG.can_move == false)
			return;
		swapRightTweens = {
			middle: {
				scale : this.game.add.tween(RPG.sprite[0].scale),
				text: this.game.add.tween(RPG.char_tab[0]),
				sprite: this.game.add.tween(RPG.sprite[0])
			}
		};

		swapRightTweens.middle.scale.to({x: 0.4, y: 0.4}, 500, Phaser.Easing.Quadratic.Out);
		swapRightTweens.middle.sprite.to({x: 575 }, 500, Phaser.Easing.Quadratic.Out);
		swapRightTweens.middle.sprite.to({y: 280 }, 500, Phaser.Easing.Quadratic.Out);
		swapRightTweens.middle.sprite.to({alpha: RPG.opacity_sprite}, 500, Phaser.Easing.Quadratic.Out);
		swapRightTweens.middle.text.to({x: 575}, 500, Phaser.Easing.Quadratic.Out);
		swapRightTweens.middle.text.to({y : 400}, 500, Phaser.Easing.Quadratic.Out);
		swapRightTweens.middle.text.to({alpha: RPG.opacity_text}, 500, Phaser.Easing.Quadratic.Out);
		
		swapRightTweens.middle.text.onComplete.add(function(){
			RPG.can_move = true;
		});

		if (RPG.char_object_list.length > 1){
			swapRightTweens.right = {
				text: this.game.add.tween(RPG.char_tab[1]),
				sprite: this.game.add.tween(RPG.sprite[1])
			}
			if(RPG.char_object_list.length == 2)
			{
				swapRightTweens.right.scale = this.game.add.tween(RPG.sprite[1].scale);
				swapRightTweens.right.scale.to({x: 0.8, y: 0.8}, 500, Phaser.Easing.Quadratic.Out);
				swapRightTweens.right.sprite.to({x: 300 }, 500, Phaser.Easing.Quadratic.Out);
				swapRightTweens.right.sprite.to({y: 210 }, 500, Phaser.Easing.Quadratic.Out);
				swapRightTweens.right.sprite.to({alpha : 1}, 500, Phaser.Easing.Quadratic.Out);
				swapRightTweens.right.text.to({x: 360 }, 500, Phaser.Easing.Quadratic.Out);
				swapRightTweens.right.text.to({y : 440 }, 500, Phaser.Easing.Quadratic.Out);
				swapRightTweens.right.text.to({alpha : 1}, 500, Phaser.Easing.Quadratic.Out);
			}
			else
			{
				swapRightTweens.right.sprite.to({x: 125 }, 500, Phaser.Easing.Quadratic.Out);
				swapRightTweens.right.text.to({x: 145}, 500, Phaser.Easing.Quadratic.Out);
			}

		}

		if (RPG.char_object_list.length > 2){
			swapRightTweens.left = {
				scale: this.game.add.tween(RPG.sprite[2].scale),
				text: this.game.add.tween(RPG.char_tab[2]),
				sprite: this.game.add.tween(RPG.sprite[2])
			}

			swapRightTweens.left.scale.to({x: 0.8, y: 0.8}, 500, Phaser.Easing.Quadratic.Out);
			swapRightTweens.left.sprite.to({x: 300}, 500, Phaser.Easing.Quadratic.Out);
			swapRightTweens.left.sprite.to({y: 210}, 500, Phaser.Easing.Quadratic.Out);
			swapRightTweens.left.sprite.to({alpha : 1}, 500, Phaser.Easing.Quadratic.Out);
			swapRightTweens.left.text.to({x: 360}, 500, Phaser.Easing.Quadratic.Out);
			swapRightTweens.left.text.to({y : 440}, 500, Phaser.Easing.Quadratic.Out);
			swapRightTweens.left.text.to({alpha : 1}, 500, Phaser.Easing.Quadratic.Out);

		}

		for (var i in swapRightTweens) {
			for (var j in swapRightTweens[i]) {
					swapRightTweens[i][j].start();
					RPG.can_move = false;
			};
		};


		switch(RPG.char_object_list.length){
			case 2:
				this.swap(RPG.char_object_list,0,1,-1);
				this.swap(RPG.char_tab,0,1,-1);
				this.swap(RPG.sprite, 0, 1, -1);
				break;
			case 3:
				this.swap(RPG.char_object_list,0,1,2);
				this.swap(RPG.char_tab,0,1,2);
				this.swap(RPG.sprite, 0, 1, 2);
				break;
			default:
				break;	
		};
		RPG.selected = RPG.char_object_list[0];
	},

	swapLeft : function(){
		if(RPG.can_move == false)
			return;
		swapLeftTweens = {
			middle: {
				scale : this.game.add.tween(RPG.sprite[0].scale),
				text: this.game.add.tween(RPG.char_tab[0]),
				sprite: this.game.add.tween(RPG.sprite[0])
			}
		};

		swapLeftTweens.middle.scale.to({x: 0.4, y: 0.4}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.middle.sprite.to({x: 125}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.middle.sprite.to({y: 280}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.middle.sprite.to({alpha: RPG.opacity_sprite}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.middle.text.to({x: 145}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.middle.text.to({y : 400}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.middle.text.to({alpha: RPG.opacity_text}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.middle.text.onComplete.add(function(){
			RPG.can_move = true;
		});
		// la rotation vers la gauche ne peut fonctionner que dans le cas où on a 3 personnages upload
		// quand on en a que 2, le 2è est toujours placé à droite
		swapLeftTweens.right = {
			text: this.game.add.tween(RPG.char_tab[1]),
			sprite: this.game.add.tween(RPG.sprite[1]),
			scale : this.game.add.tween(RPG.sprite[1].scale)
		}
		swapLeftTweens.right.scale.to({x: 0.8, y: 0.8}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.right.sprite.to({x: 300}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.right.sprite.to({y: 210}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.right.sprite.to({alpha : 1}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.right.text.to({x: 360}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.right.text.to({y: 440}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.right.text.to({alpha : 1}, 500, Phaser.Easing.Quadratic.Out);

		swapLeftTweens.left = {
			scale: this.game.add.tween(RPG.sprite[2].scale),
			text: this.game.add.tween(RPG.char_tab[2]),
			sprite: this.game.add.tween(RPG.sprite[2])
		}

		swapLeftTweens.left.scale.to({x: 0.4, y: 0.4}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.left.sprite.to({x: 575}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.left.sprite.to({alpha : RPG.opacity_sprite}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.left.text.to({x: 575}, 500, Phaser.Easing.Quadratic.Out);
		swapLeftTweens.left.text.to({alpha : RPG.opacity_text}, 500, Phaser.Easing.Quadratic.Out);

		for (var i in swapLeftTweens) {
			for (var j in swapLeftTweens[i]) {
					swapLeftTweens[i][j].start();
					RPG.can_move = false;
			};
		};

		switch(RPG.char_object_list.length){
			case 3:
				this.swap(RPG.char_object_list,0,2,1);
				this.swap(RPG.char_tab,0,2,1);
				this.swap(RPG.sprite,0,2,1);
				break;
			default:
				break;
		};
		RPG.selected = RPG.char_object_list[0];
	},

	createText: function() {
		RPG.socket.emit('get_characters_account',sessionStorage.getItem('username'));

		var self = this;
		RPG.socket.on('return_characters_account',function(data){
			var characters_account = JSON.parse(data);
			RPG.char_object_list = characters_account;
			RPG.nb_char_find = characters_account.length;
			if(RPG.nb_char_find != 0)
			{
				self.createListCharacter(RPG.char_object_list);
			}
			else
			{
				self.createListCharacter('None');
			}
		});

		button_start = this.game.add.button( 530, 20, 'bouton_start', this.loadCharAction,this);
		button_create = this.game.add.button( 20, 20, 'bouton_create', this.createCharAction,this);
		button_change_R = this.game.add.button(687,286,'fleche',this.swapRight,this);
		button_change_L = this.game.add.button(0,293,'fleche_L',this.swapLeft,this);
		button_delete = this.game.add.button(650,560,'bouton_delete',this.deleteCharAction,this);
	},
};