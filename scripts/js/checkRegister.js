var username_ok ;
var password_ok ;
var mail_ok;

function init()
{
	username_ok = false;
	password_ok = false;
	mail_ok = false;
}

function checkInput(champ)
{
	if(champ.value.length > 2 && champ.value.length < 31) // pseudo et mot de passe d'au moins 3 caractères et de 30 au plus
	{
		document.getElementById('message').style.visibility = 'hidden';
		document.getElementById('message').innerHTML = "";
		if(champ.id == "username"){username_ok = true;}
		else {password_ok = true;}
	}
	else
	{
		document.getElementById('message').style.visibility = 'visible';
		document.getElementById('message').innerHTML = "<img src='../assets/images/errorimg.png' class='error_img' /> Username and password between 3-30 characters ! ";
		if(champ.id == "username"){username_ok = false;}
		else {password_ok = false;}
	}
}

function checkFormLogin()
{
	if(username_ok == true && password_ok == true)
	{
		document.getElementById('message').style.visibility = 'hidden';
		document.getElementById('message').innerHTML = "";
		return true;
	}
	else 
	{
		document.getElementById('message').style.visibility = 'visible';
		document.getElementById('message').innerHTML = "<img src='../assets/images/errorimg.png' class='error_img' /> One input is not correctly completed !";
		return false;
	}
}

function checkMail(champ)
{
	var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');

	if(reg.test(champ.value))
	{
		mail_ok = true;
		document.getElementById('message').style.visibility = 'hidden';
		document.getElementById('message').innerHTML = "";
		return true;
	}
	else
	{
		mail_ok = false;
		document.getElementById('message').style.visibility = 'visible';
		document.getElementById('message').innerHTML = "<img src='../assets/images/errorimg.png' class='error_img' /> Problem with the format of your mail address ";
		return false;
	}
}

function checkFormRegister()
{
	if(username_ok == true && password_ok == true && mail_ok == true)
	{
		document.getElementById('message').style.visibility = 'hidden';
		document.getElementById('message').innerHTML = "";
		return true;
	}
	else
	{
		document.getElementById('message').style.visibility = 'visible';
		document.getElementById('message').innerHTML = "<img src='../assets/images/errorimg.png' class='error_img' /> One input is not correctly completed !";
		return false;
	}
}