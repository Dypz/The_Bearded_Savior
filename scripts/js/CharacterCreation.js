RPG.CharacterCreation = function(game) {
	RPG.all_pseudo = new Array();
	RPG.choosen_job = null;
	RPG.can_move = true;
};

RPG.CharacterCreation.prototype = {
	preload: function() {
		this.background = this.add.tileSprite(0, 0, 800, 600, 'CCBackground');
	},

	create: function() {
		this.createText();
		RPG.socket.emit('get_all_characters');

		RPG.socket.on('return_all_characters',function(data){
			var all_characters = JSON.parse(data);
			for(var i=0;i<all_characters.length;i++)
			{
				RPG.all_pseudo.push(all_characters[i].name);
			}
		});
		this.input.keyboard.addCallbacks(this, null, null, this.keyPress);
	},

	downBerseker: function() {
		if(RPG.can_move == false || RPG.choosen_job == 'Berseker')
			return;
		swapJobTweens = {
			berseker: {
				scale : this.game.add.tween(this.berseker_choice.scale),
				button: this.game.add.tween(this.berseker_choice)
			}
		};

		swapJobTweens.berseker.scale.to({x: 1, y: 1}, 500, Phaser.Easing.Quadratic.Out);
		swapJobTweens.berseker.button.to({x: '-225'}, 500, Phaser.Easing.Quadratic.Out);
		swapJobTweens.berseker.button.to({y: '+85'}, 500, Phaser.Easing.Quadratic.Out);
		swapJobTweens.berseker.button.to({alpha: 1}, 500, Phaser.Easing.Quadratic.Out);

		swapJobTweens.berseker.button.onComplete.add(function(){
			RPG.can_move = true;
		});

		swapJobTweens.neogician = {
			scale : this.game.add.tween(this.neogician_choice.scale),
			button : this.game.add.tween(this.neogician_choice)
		};
		if(RPG.choosen_job != null)
		{
			swapJobTweens.neogician.button.to({x: 50}, 500, Phaser.Easing.Quadratic.Out);
			swapJobTweens.neogician.button.to({y: 120}, 500, Phaser.Easing.Quadratic.Out);
		}
		swapJobTweens.neogician.scale.to({x: 0.5, y: 0.5}, 500, Phaser.Easing.Quadratic.Out);
		swapJobTweens.neogician.button.to({alpha: 0.5}, 500, Phaser.Easing.Quadratic.Out);

		for (var i in swapJobTweens) {
			for (var j in swapJobTweens[i]) {
					swapJobTweens[i][j].start();
					RPG.can_move = false;
			};
		};
		RPG.choosen_job = 'Berseker';
	},

	
	downNeogician: function() {
		if(RPG.can_move == false || RPG.choosen_job == 'Neogician')
			return;
		swapJobTweens = {
			berseker: {
				scale : this.game.add.tween(this.berseker_choice.scale),
				button: this.game.add.tween(this.berseker_choice)
			}
		};
		if(RPG.choosen_job != null)
		{
			swapJobTweens.berseker.button.to({x: 500}, 500, Phaser.Easing.Quadratic.Out);
			swapJobTweens.berseker.button.to({y: 100}, 500, Phaser.Easing.Quadratic.Out);
		}
		swapJobTweens.berseker.scale.to({x: 0.5, y: 0.5}, 500, Phaser.Easing.Quadratic.Out);
		swapJobTweens.berseker.button.to({alpha: 0.5}, 500, Phaser.Easing.Quadratic.Out);

		swapJobTweens.berseker.button.onComplete.add(function(){
			RPG.can_move = true;
		});

		swapJobTweens.neogician = {
			scale : this.game.add.tween(this.neogician_choice.scale),
			button : this.game.add.tween(this.neogician_choice)
		};
		swapJobTweens.neogician.button.to({x: '+225'}, 500, Phaser.Easing.Quadratic.Out);
		swapJobTweens.neogician.button.to({y: '+85'}, 500, Phaser.Easing.Quadratic.Out);
		swapJobTweens.neogician.scale.to({x: 1, y: 1}, 500, Phaser.Easing.Quadratic.Out);
		swapJobTweens.neogician.button.to({alpha: 1}, 500, Phaser.Easing.Quadratic.Out);

		for (var i in swapJobTweens) {
			for (var j in swapJobTweens[i]) {
					swapJobTweens[i][j].start();
					RPG.can_move = false;
			};
		};
		RPG.choosen_job = 'Neogician';
	},

	downP: function() {
		var autorised = true;
		for(var i=0;i<RPG.all_pseudo.length;i++)
		{
			if(RPG.all_pseudo[i] == this.pseudoStr)
			{
				alert('Pseudo already used, choose another ! ');
				autorised = false;
				break;
			}
		}
		if(this.pseudoStr == '')
		{
			alert('Choose a pseudo ! ');
			autorised = false;
		}
		if(RPG.choosen_job == null)
		{
			alert('Choose a job ! ');
			autorised = false;
		}
		if(autorised)
		{
			sessionStorage.setItem("pseudo",this.pseudoStr);
			this.state.start('Game');
			this.state.start('Intro');
		}
	},

	createText: function() {
		var textStyle = {
			font: "37px Lucida Console",
			fill: "white",
			align: "center"
		};

		this.start_cc = this.game.add.button( 600, 550, 'start_cc',this.downP,this);
		this.berseker_choice= this.game.add.button( 500, 100, 'berseker');
		this.berseker_choice.inputEnabled = true;
		this.berseker_choice.events.onInputDown.add(this.downBerseker, this);
		this.neogician_choice= this.game.add.button( 50, 120, 'neogician');
		this.neogician_choice.inputEnabled = true;
		this.neogician_choice.events.onInputDown.add(this.downNeogician, this);
		this.pseudoStr = new String();
		this.pseudo = this.add.text(this.world.centerX-20, this.world.centerY+180, this.pseudoStr, textStyle);
		this.pseudo.anchor.set(0.5);
	},

	keyPress: function(chara) {
		if (chara !== "\b") {
			this.pseudoStr += chara;
		}
		else {
			this.pseudoStr = this.pseudoStr.slice(0, -1);
		}
		this.pseudo.text = this.pseudoStr
	},

	update: function() {
		
	}
};