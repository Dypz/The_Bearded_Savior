// character class
function Character(game) {
	this.game = game;
	this.id = null; // the id will be use to authentificate the player
	this.session= "";
	// name of the character
	this.name = "";
	/* several jobs: Beginner, Mechsoldier, Cyberescamotor, Sniper, Neogician, Enemy
	 * Neogician : Use technology
	 * Mechsoldier: Heavy weapons, slow
	 * Cyberescamotor: furtive and deceitful
	 * Sniper: fast, long range, not many hp
	 */
	this.job = new String();
	// level of the character (max: 50)
	this.level;
	this.levelUpCount = 0;
	// experience points
	this.exp;
	this.expPercent;
	this.abilities = new Array(6);
	// characteristics
	this.strength;
	this.dexterity;
	this.intelligence;
	this.stamina;
	this.maxHealth = 100;
	this.currentHealth;
	// movement speed (pixels per sec)
	this.moveSpeed = 75;
	// sprite of character
	this.sprite = null;
	// is alive
	this.alive = true;
	// death count
	this.deathCount = 0;
  	this.tilex = 0;
  	this.tiley = 0;
  	this.orientation = 1;
  	this.movable;
  	this.map;
  	// fonts
  	this.isSkillsDisplayed = false;
  	this.isProfileDisplayed = false;
  	this.fireRate = 1000;
    this.nextFire = 0;
    this.isTalking = false;
    this.spell1Time = 0;
    this.spell2Time = 0;
    this.spell3Time = 0;
    this.spell4Time = 0;
    this.spell4Activation = false;
    this.spell4ActivationTime = 750;
    this.spell4NextFire = 0;
    this.spell5Time = 0;
    this.spell5Activation = false;
    this.spell5ActivationTime = 5000;
    this.spell6Time = 0;
    this.baseDamages = {min: 1, max: 1};
    this.baseStamina;
}

Character.prototype = {
	preload : function(){
		this.game.load.spritesheet('neogician_sprite' , './assets/sprites/character/neogician_sprite.png', 32, 48, 16);
		this.game.load.spritesheet('berseker_sprite' , './assets/sprites/character/scientifique.png', 32, 48, 16);
	},
	create: function(x,y,job,name,level,exp,expPercent,currentHealth,strength,intelligence,stamina,dexterity,map,direction,orientation,send_to_others) {
	  	//Permet d'utiliser les touches du clavier

	    this.cursors = this.game.input.keyboard.createCursorKeys();
	    this.action = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
	    this.talk = this.game.input.keyboard.addKey(Phaser.Keyboard.E);
	    this.shortcutStrength = this.game.input.keyboard.addKey(Phaser.Keyboard.U);
	    this.shortcutIntelligence = this.game.input.keyboard.addKey(Phaser.Keyboard.I);
	    this.shortcutDexterity = this.game.input.keyboard.addKey(Phaser.Keyboard.O);
	    this.shortcutStamina = this.game.input.keyboard.addKey(Phaser.Keyboard.P);
	    this.skill1 = this.game.input.keyboard.addKey(Phaser.Keyboard.ONE);
	    this.skill2 = this.game.input.keyboard.addKey(Phaser.Keyboard.TWO);
	    this.skill3 = this.game.input.keyboard.addKey(Phaser.Keyboard.THREE);
	    this.skill4 = this.game.input.keyboard.addKey(Phaser.Keyboard.FOUR);
	    this.skill5 = this.game.input.keyboard.addKey(Phaser.Keyboard.FIVE);
	    this.skill6 = this.game.input.keyboard.addKey(Phaser.Keyboard.SIX);

		this.shortcutStrength.onDown.add(this.upStrength, this);
		this.shortcutIntelligence.onDown.add(this.upIntelligence, this);
		this.shortcutDexterity.onDown.add(this.upDexterity, this);
		this.shortcutStamina.onDown.add(this.upStamina, this);
		 //Création de notre personnage
		if(job == 'Neogician')
		{
			this.sprite = this.game.add.sprite(x,y,'neogician_sprite');
		}
		else
		{
			this.sprite = this.game.add.sprite(x,y,'berseker_sprite');
		}
		this.sprite.anchor.setTo(0.5,0.5);
	    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
		//Chargeons les animations du joueur
	    this.sprite.direction = direction;
	    this.sprite.animations.add('down', [0, 1, 2, 3], 6, true , true);	//2
		this.sprite.animations.add('left', [4, 5, 6, 7], 6, true , true);	//3
	    this.sprite.animations.add('right', [8, 9, 10, 11], 6, true , true);	//4
		this.sprite.animations.add('up', [12, 13, 14, ], 6, true , true);	//1
		
		//La caméra est centrée sur le joueur
		if(send_to_others == true)
			this.game.camera.follow(this.sprite);

		//Collision sur les bords
		this.sprite.body.collideWorldBounds = true;

		this.setJob(job)
	    this.setMap(map);
	    this.setName(name);
	    this.setLevel(level);
	    this.setExp(exp);
	    this.setExpPercent(expPercent);
	    this.setCurrentHealth(currentHealth);
	    this.setStrength(strength);
	    this.setIntelligence(intelligence);
	    this.setStamina(stamina);
	    this.setDexterity(dexterity);
	    this.setSession(sessionStorage.getItem('username'));
	    this.setOrientation(orientation);
	    this.setSkills();
	    this.setMovable(send_to_others);
	    if(send_to_others == true)
			RPG.socket.emit('new_player',{x : x,y : y, job : this.job,name : this.name, level : this.level, exp : this.exp, orientation : this.orientation,direction : this.sprite.direction, frame : this.sprite.frame, strength : this.strength, dexterity : this.dexterity, intelligence : this.intelligence, stamina : this.stamina, maxHealth : this.maxHealth, currentHealth : this.currentHealth,map : this.map});
		var self = this;
	    RPG.socket.on('send_id_player',function(data){
	    	self.setId(data.id);
	    	console.log('id player : '+self.getId());
	    	RPG.id = self.getId();
	    });

	},
	update: function(bullets, spell1, spell3, spell6) {
		if (this.isTalking == false)
		{
			if(this.movable == true)
			{
				this.movements();
				if (this.alive)
				{
					if (this.levelUpCount > 0) {
						
					}
				    this.fire(bullets);
					this.castSpells(bullets, spell1, spell3, spell6);			
				    this.isAlive();
				    if (this.spell4Activation == true)
				    {
				    	this.scatterFire(bullets);
				    }
				    if (this.spell5Activation == true)
				    {
				    	this.shieldUpdate();	
				    }
				}
			}	
		}
	},
	setSkills: function() {
		// laser
		this.abilities[0] = new Ability(this.game);
		this.abilities[0].setName("Missile");
		this.abilities[0].setBaseMinDamage(10);
		this.abilities[0].setBaseMaxDamage(20);
		this.abilities[0].cooldown = 4000;
		// heal
		this.abilities[1] = new Ability(this.game);
		this.abilities[1].setName("Heal");
		this.abilities[1].setBaseMinDamage(10);
		this.abilities[1].setBaseMaxDamage(15);
		this.abilities[1].cooldown = 10000;
		// fire
		this.abilities[2] = new Ability(this.game);
		this.abilities[2].setName("Fire");
		this.abilities[2].setBaseMinDamage(4);
		this.abilities[2].setBaseMaxDamage(6);
		this.abilities[2].cooldown = 3000;
		this.abilities[2].range = 100;
		// auto fire
		this.abilities[3] = new Ability(this.game);
		this.abilities[3].setName("Auto-Fire");
		this.abilities[3].setBaseMinDamage(1);
		this.abilities[3].setBaseMaxDamage(2);
		this.abilities[3].cooldown = 12000;
		// shield
		this.abilities[4] = new Ability(this.game);
		this.abilities[4].setName("Shield");
		this.abilities[4].setBaseMinDamage(10);
		this.abilities[4].setBaseMaxDamage(15);
		this.abilities[4].cooldown = 6000;
		// slash
		this.abilities[5] = new Ability(this.game);
		this.abilities[5].setName("Slash");
		this.abilities[5].setBaseMinDamage(5);
		this.abilities[5].setBaseMaxDamage(7);
		this.abilities[5].cooldown = 1500;
		this.abilities[5].range = 35;
	},
	castSpells: function(bullets, spell1, spell3, spell6) {
		if (this.skill1.isDown)	{
	  		if (this.game.time.now > this.spell1Time) {
	  			this.spell1Now = this.game.time.now;
	    		this.baseDamages['min'] = this.abilities[0].getBaseMinDamage()*(1+(0.1*this.intelligence));
	    		this.baseDamages['max'] = this.abilities[0].getBaseMaxDamage()*(1+(0.1*this.intelligence));
				this.animateSpell1(spell1);
		        this.spell1Time = this.game.time.now + this.abilities[0].cooldown;
		        RPG.socket.emit('spell_launched',{id : this.getId(), type : 'spell1'});
	    	}
	    }
	    else if (this.skill2.isDown) {
	    	if (this.game.time.now > this.spell2Time) {
	    		this.spell2Now = this.game.time.now;
	    		this.baseDamages['min'] = this.abilities[1].getBaseMinDamage()*(1+(0.1*this.intelligence));
	    		this.baseDamages['max'] = this.abilities[1].getBaseMaxDamage()*(1+(0.1*this.intelligence));
	    		var healValue = Math.round(Math.random()*(this.baseDamages['max'] - this.baseDamages['min'])+this.baseDamages['min']);
	    		console.log('healed ' + healValue);
	    		this.modifyCurrentHealth(healValue);
	    		this.spell2Time = this.game.time.now + this.abilities[1].cooldown;
	    		this.animateSpell2();
    			var dmgText = this.game.add.text(this.sprite.x-8, this.sprite.y-32, healValue);
				dmgText.font = 'Roboto Slab';
				dmgText.fontSize = 20;
				dmgText.align = 'center';
				dmgText.stroke = '#000000';
				var grd = dmgText.context.createLinearGradient(0, 0, 0, dmgText.canvas.height);
    			grd.addColorStop(0, '#71EE6D');   
				grd.addColorStop(1, '#4FA64C');
				dmgText.fill = grd;
				this.game.time.events.add(100, function() {
				    this.game.add.tween(dmgText).to({y: this.sprite.y-200}, 3000, Phaser.Easing.Linear.None, true);
				    this.game.add.tween(dmgText).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
				}, this);
				RPG.socket.emit('spell_launched',{id : this.getId(), type : 'spell2'});
	    	}
	    }
	    else if (this.skill3.isDown) {
	    	if (this.game.time.now > this.spell3Time) {
	    		this.spell3Now = this.game.time.now;
	    		this.baseDamages['min'] = this.abilities[2].getBaseMinDamage()*(1+(0.15*this.intelligence));
	    		this.baseDamages['max'] = this.abilities[2].getBaseMaxDamage()*(1+(0.15*this.intelligence));
	    		this.animateSpell3(spell3);
    			this.spell3Time = this.game.time.now + this.abilities[2].cooldown;
    			RPG.socket.emit('spell_launched',{id : this.getId(), type : 'spell3'});
	    	}
	    }
	    else if (this.skill4.isDown) {
	  		if (this.game.time.now > this.spell4Time) {
	  			this.spell4Now = this.game.time.now;
	    	    this.spell4Activation = true;
		        this.spell4Time = this.game.time.now + this.abilities[3].cooldown;
		        RPG.socket.emit('spell_launched',{id : this.getId(), type : 'spell4'});
	    	}
	    }
	    else if (this.skill5.isDown) {
	    	if (this.game.time.now > this.spell5Time) {
	    		this.spell5Now = this.game.time.now;
	    		this.spell5Time = this.game.time.now + this.abilities[4].cooldown;
	    		this.animateSpell5();
	    		RPG.socket.emit('spell_launched',{id : this.getId(), type : 'spell5'});
	    	}
	    }
	    else if (this.skill6.isDown) {
	    	if (this.game.time.now > this.spell6Time) {
	    		this.spell6Now = this.game.time.now;
	    		this.baseDamages['min'] = this.abilities[5].getBaseMinDamage()*(1+(0.15*this.strength));
	    		this.baseDamages['max'] = this.abilities[5].getBaseMaxDamage()*(1+(0.15*this.strength));
	    		this.animateSpell6();
				this.spell6Time = this.game.time.now + this.abilities[5].cooldown;
				RPG.socket.emit('spell_launched',{id : this.getId(), type : 'spell6'});
	    	}
	    }
	},
	animateSpell1: function(spell1) {
		var spell = spell1.getFirstExists(false);
		console.log('spell : '+spell);
		spell.lifespan = 2000;
		this.game.physics.arcade.enable(spell);
        switch(true) {
       	case (this.orientation == 1): 
       	    spell.reset(this.sprite.x, this.sprite.y-18);
       		spell.rotation = (-Math.PI); 
       		spell.body.velocity.y = -600;
       		break;
       	case (this.orientation == 2): 
       	    spell.reset(this.sprite.x-8, this.sprite.y);
       		spell.rotation = (Math.PI)/2; 
       		spell.body.velocity.x = -600;
       		break;
      	case (this.orientation == 3): 
       	    spell.reset(this.sprite.x, this.sprite.y+18);
       		spell.rotation = 0; 
       		spell.body.velocity.y = 600;
       		break;
	   	case (this.orientation == 4): 
			spell.reset(this.sprite.x+8, this.sprite.y);
		    spell.rotation = (-Math.PI)/2; 
			spell.body.velocity.x = 600;
			break;
		}
	},
	animateSpell2: function() {
		var heal = this.game.add.sprite(-32, -36, 'heal');
	    this.sprite.addChild(heal);
    	heal.animations.add('healing', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]);
    	heal.play('healing', 20, false, true);
	},
	animateSpell3: function(spell3) {
	   	var spell = spell3.getFirstExists(false);
		spell.lifespan = 2000;
		this.game.physics.arcade.enable(spell);
		switch (true) {
			case (this.orientation == 1):
		   		spell.reset(this.sprite.x, this.sprite.y - this.abilities[2].range);
		   		break;
		   	case (this.orientation == 2):
		   		spell.reset(this.sprite.x - this.abilities[2].range, this.sprite.y);
		   		break;
		   	case (this.orientation == 3):
		   		spell.reset(this.sprite.x, this.sprite.y + this.abilities[2].range);
		   		break;
		   	case (this.orientation == 4):
		   		spell.reset(this.sprite.x + this.abilities[2].range, this.sprite.y);
		   		break;
		}
    	spell.animations.add('fire', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]);
    	spell.play('fire', 9, false, true);
	},
	animateSpell4: function(bullets) {
		console.log('spell4');
		var bullet = bullets.getFirstExists(false);
		bullet.lifespan = 3000;
		this.game.physics.arcade.enable(bullet);
		switch(true)
		{
		   	case (this.orientation == 1): 
		   	    bullet.reset(this.sprite.x + this.game.rnd.between(-10, 10), this.sprite.y-18);
		   		bullet.rotation = (-Math.PI)/2; 
		   		bullet.body.velocity.y = -300;
		   		break;
		   	case (this.orientation == 2): 
		   	    bullet.reset(this.sprite.x-8, this.sprite.y + this.game.rnd.between(-10, 10));
		   		bullet.rotation = (-Math.PI); 
		   		bullet.body.velocity.x = -300 ;
		   		break;
		   	case (this.orientation == 3): 
		   	    bullet.reset(this.sprite.x + this.game.rnd.between(-10, 10), this.sprite.y+18);
		   		bullet.rotation = (Math.PI)/2; 
		   		bullet.body.velocity.y = 300;
		   		break;
		   	case (this.orientation == 4): 
		   	    bullet.reset(this.sprite.x+8, this.sprite.y + this.game.rnd.between(-10, 10));
		   	    bullet.rotation = 0; 
		   		bullet.body.velocity.x = 300;
		   		break;
		}
	},
	animateSpell5: function() {
		this.shield = this.game.add.sprite(-98 , -80 , 'shield');
	    this.sprite.addChild(this.shield);
    	this.shield.animations.add('energy_shield_open' , [1,2,3,4,5,6,7,8,9,10,11,12]);
    	this.shield.animations.play('energy_shield_open' , 25, false , true);
    	this.spell5Activation = true;
    	this.game.time.events.add(500, this.shieldCreate, this);
	},
	animateSpell6: function() {
		switch (true) {
	    	case (this.orientation == 1):
	    		this.spell6Sprite = this.game.add.sprite(-32, -1*this.abilities[5].range-32, 'slash');
	       		break;
	       	case (this.orientation == 2):
	       		this.spell6Sprite = this.game.add.sprite(-1*this.abilities[5].range-32, -32, 'slash');
	       		break;
	       	case (this.orientation == 3):
	       		this.spell6Sprite = this.game.add.sprite(-1*this.abilities[5].range, 0, 'slash');
	       		break;
	       	case (this.orientation == 4):
	       		this.spell6Sprite = this.game.add.sprite(0, -1*this.abilities[5].range, 'slash');
	       		break;
	    }
	    this.game.physics.arcade.enable(this.spell6Sprite);
	    this.sprite.addChild(this.spell6Sprite);
	    this.spell6Sprite.animations.add('slash', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]);
		this.spell6Sprite.play('slash', 15, false, true);
	},
	addInterface: function() {
		this.nameText = this.interfaceAddName();
		this.healthText = this.interfaceAddHealth();
		this.healthText.fixedToCamera = true;
		this.expText = this.interfaceAddExp();
		this.expText.fixedToCamera = true;
		this.interfaceAddMenus();		
		this.interfaceAddUpText();
		this.interfaceAddSpells();
	},
	updateInterface: function() {
		this.interfaceUpdateHealth();
		this.interfaceUpdateExp();
		this.interfaceUpdateName();
	    this.interfaceUpdateMenus();
	    this.interfaceUpdateSpells();
	},
	interfaceAddName: function() {
		var x = this.sprite.x;
		var y = this.sprite.y;
		var font = {font: "15px Roboto Slab", align: "center", stroke: "#000000", fill: "#DAFFF9"};
		return this.game.add.text(x, y, this.getName(), font);
	},
	interfaceAddSpells: function() {
		var font = {font: "15px Roboto Slab", align: "center", stroke: "#000000"};
		// spell 1
		this.spell1CD = this.game.add.graphics(58, RPG.gameWidth-52);
		this.spell1CD.fixedToCamera = true;
		this.spell1Text = this.game.add.text(5, RPG.gameWidth-65, this.abilities[0].getName() + ' ', font);
		this.spell1Text.fixedToCamera = true;
		var grd = this.spell1Text.context.createLinearGradient(0, 0, 0, this.spell1Text.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		this.spell1Text.fill = grd;
		// spell 2
		this.spell2CD = this.game.add.graphics(135, RPG.gameWidth-52);
		this.spell2CD.fixedToCamera = true;
		this.spell2Text = this.game.add.text(100, RPG.gameWidth-65, this.abilities[1].getName() + ' ', font);
		this.spell2Text.fixedToCamera = true;
		grd = this.spell2Text.context.createLinearGradient(0, 0, 0, this.spell2Text.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		this.spell2Text.fill = grd;
		// spell 3
		this.spell3CD = this.game.add.graphics(215, RPG.gameWidth-52);
		this.spell3CD.fixedToCamera = true;
		this.spell3Text = this.game.add.text(184, RPG.gameWidth-65, this.abilities[2].getName() + ' ', font);
		this.spell3Text.fixedToCamera = true;
		grd = this.spell3Text.context.createLinearGradient(0, 0, 0, this.spell3Text.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		this.spell3Text.fill = grd;
		// spell 4
		this.spell4CD = this.game.add.graphics(330, RPG.gameWidth-52);
		this.spell4CD.fixedToCamera = true;
		this.spell4Text = this.game.add.text(260, RPG.gameWidth-65, this.abilities[3].getName() + ' ', font);
		this.spell4Text.fixedToCamera = true;
		grd = this.spell4Text.context.createLinearGradient(0, 0, 0, this.spell4Text.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		this.spell4Text.fill = grd;
		// spell 5
		this.spell5CD = this.game.add.graphics(425, RPG.gameWidth-52);
		this.spell5CD.fixedToCamera = true;
		this.spell5Text = this.game.add.text(375, RPG.gameWidth-65, this.abilities[4].getName() + ' ', font);
		this.spell5Text.fixedToCamera = true;
		grd = this.spell5Text.context.createLinearGradient(0, 0, 0, this.spell5Text.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		this.spell5Text.fill = grd;
		// spell 6
		this.spell6CD = this.game.add.graphics(517, RPG.gameWidth-52);
		this.spell6CD.fixedToCamera = true;
		this.spell6Text = this.game.add.text(475, RPG.gameWidth-65, this.abilities[5].getName() + ' ', font);
		this.spell6Text.fixedToCamera = true;
		grd = this.spell6Text.context.createLinearGradient(0, 0, 0, this.spell6Text.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		this.spell6Text.fill = grd;
		// update
		this.interfaceUpdateSpells();
	},
	interfaceUpdateSpells: function() {
		if (this.game.time.now < this.spell1Time) {
			this.spell1CD.clear();
			var x = (this.game.time.now / this.spell1Time) * 100;
			var color = this.rgbToHex((x > 50 ? 1-2*(x-50)/100.0 : 1.0) * 255, (x > 50 ? 1.0 : 2*x/100.0) * 255, 0);

			this.spell1CD.beginFill(color);
			this.spell1CD.lineStyle(5, color, 1);
			this.spell1CD.moveTo(0,-5);
			this.spell1CD.lineTo((40) * (this.game.time.now-this.spell1Now) / (this.spell1Time-this.spell1Now), -5);
			this.spell1CD.endFill();
		}
		else {
			this.spell1CD.clear();
		}
		if (this.game.time.now < this.spell2Time) {
			this.spell2CD.clear();
			var x = (this.game.time.now / this.spell2Time) * 100;
			var color = this.rgbToHex((x > 50 ? 1-2*(x-50)/100.0 : 1.0) * 255, (x > 50 ? 1.0 : 2*x/100.0) * 255, 0);

			this.spell2CD.beginFill(color);
			this.spell2CD.lineStyle(5, color, 1);
			this.spell2CD.moveTo(0,-5);
			this.spell2CD.lineTo((40) * (this.game.time.now-this.spell2Now) / (this.spell2Time-this.spell2Now), -5);
			this.spell2CD.endFill();
		}
		else {
			this.spell2CD.clear();
		}
		if (this.game.time.now < this.spell3Time) {
			this.spell3CD.clear();
			var x = (this.game.time.now / this.spell3Time) * 100;
			var color = this.rgbToHex((x > 50 ? 1-2*(x-50)/100.0 : 1.0) * 255, (x > 50 ? 1.0 : 2*x/100.0) * 255, 0);

			this.spell3CD.beginFill(color);
			this.spell3CD.lineStyle(5, color, 1);
			this.spell3CD.moveTo(0,-5);
			this.spell3CD.lineTo((40) * (this.game.time.now-this.spell3Now) / (this.spell3Time-this.spell3Now), -5);
			this.spell3CD.endFill();
		}
		else {
			this.spell3CD.clear();
		}
		if (this.game.time.now < this.spell4Time) {
			this.spell4CD.clear();
			var x = (this.game.time.now / this.spell4Time) * 100;
			var color = this.rgbToHex((x > 50 ? 1-2*(x-50)/100.0 : 1.0) * 255, (x > 50 ? 1.0 : 2*x/100.0) * 255, 0);

			this.spell4CD.beginFill(color);
			this.spell4CD.lineStyle(5, color, 1);
			this.spell4CD.moveTo(0,-5);
			this.spell4CD.lineTo((40) * (this.game.time.now-this.spell4Now) / (this.spell4Time-this.spell4Now), -5);
			this.spell4CD.endFill();
		}
		else {
			this.spell4CD.clear();
		}
		if (this.game.time.now < this.spell5Time) {
			this.spell5CD.clear();
			var x = (this.game.time.now / this.spell5Time) * 100;
			var color = this.rgbToHex((x > 50 ? 1-2*(x-50)/100.0 : 1.0) * 255, (x > 50 ? 1.0 : 2*x/100.0) * 255, 0);

			this.spell5CD.beginFill(color);
			this.spell5CD.lineStyle(5, color, 1);
			this.spell5CD.moveTo(0,-5);
			this.spell5CD.lineTo((40) * (this.game.time.now-this.spell5Now) / (this.spell5Time-this.spell5Now), -5);
			this.spell5CD.endFill();
		}
		else {
			this.spell5CD.clear();
		}
		if (this.game.time.now < this.spell6Time) {
			this.spell6CD.clear();
			var x = (this.game.time.now / this.spell6Time) * 100;
			var color = this.rgbToHex((x > 50 ? 1-2*(x-50)/100.0 : 1.0) * 255, (x > 50 ? 1.0 : 2*x/100.0) * 255, 0);

			this.spell6CD.beginFill(color);
			this.spell6CD.lineStyle(5, color, 1);
			this.spell6CD.moveTo(0,-5);
			this.spell6CD.lineTo((40) * (this.game.time.now-this.spell6Now) / (this.spell6Time-this.spell6Now), -5);
			this.spell6CD.endFill();
		}
		else {
			this.spell6CD.clear();
		}
	},
	interfaceAddHealth: function() {
		var healthStr = "Health ";
		this.healthbar = this.game.add.graphics(63, RPG.gameWidth-12);
		this.healthbar.fixedToCamera = true;
		var font = {font: "15px Roboto Slab", align: "center", stroke: "#000000"};
		var text = this.game.add.text(5, RPG.gameWidth-25, healthStr, font);
		var grd = text.context.createLinearGradient(0, 0, 0, text.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		text.fill = grd;
		this.interfaceUpdateHealth();
		return text;
	},
	interfaceAddExp: function() {
		var expStr = "Level " + this.getLevel();
		this.expbar = this.game.add.graphics(63, RPG.gameWidth-32);
		this.expbar.fixedToCamera = true;
		var font = {font: "15px Roboto Slab", align: "center", stroke: "#000000"};
		var text = this.game.add.text(5, RPG.gameWidth-45, expStr, font);
		var grd = text.context.createLinearGradient(0, 0, 0, text.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		text.fill = grd;
		this.interfaceUpdateExp();
		return text;
	},
	interfaceAddMenus: function() {
		banderoleGroup = this.game.add.group();
		banderoleGroup.cameraOffset.y = -100;
		banderoleGroup.z=500
		banderoleGroup.fixedToCamera = true;

		banderole = this.game.add.sprite(7,0,'banderole');
		
		// profile related interface
		button_profil = this.game.add.button( 166, 15, 'profil',this.interfaceDisplayProfile, this);
		button_skill = this.game.add.button( 330, 15, 'skill',this.interfaceDisplaySkills, this);
		button_save = this.game.add.button( 454, 16, 'save',this.interfaceDisplaySave, this);
		button_quit = this.game.add.button( 580, 17, 'quit',this.interfaceDisplayQuit, this);

		banderoleGroup.add(banderole);
		banderoleGroup.add(button_profil);
		banderoleGroup.add(button_skill);
		banderoleGroup.add(button_save);
		banderoleGroup.add(button_quit);

		banderoleToggleTween = this.game.add.tween(banderoleGroup.cameraOffset);
		banderoleCloseTween = this.game.add.tween(banderoleGroup.cameraOffset);

		banderoleToggleTween.to({y: 0}, 1000, Phaser.Easing.Bounce.Out);
		banderoleCloseTween.to({y: -100}, 500, Phaser.Easing.Linear.None);
	},
	interfaceUpdateMenus: function() {
		var mouseX = this.game.input.mousePointer.x;
		var mouseY = this.game.input.mousePointer.y;
		if (mouseX > 150 && mouseX <= RPG.gameLength-150 && mouseY < 50 && mouseY > 0) {
			banderoleToggleTween.start();
			
		}
		else {
			banderoleCloseTween.start();
		}
	},
	interfaceUpdateName: function() {
		this.nameText.x = this.sprite.x-(this.name.length+10);
	    this.nameText.y = this.sprite.y-32;
	},
	interfaceUpdateHealth: function() {
		if (this.getCurrentHealth() >= 0) {
		    this.healthbar.clear();
		    var x = (this.getCurrentHealth() / this.getMaxHealth()) * 100;
		    var color = this.rgbToHex((x > 50 ? 1-2*(x-50)/100.0 : 1.0) * 255, (x > 50 ? 1.0 : 2*x/100.0) * 255, 0);
		    
		    this.healthbar.beginFill(color);
		    this.healthbar.lineStyle(5, color, 1);
		    this.healthbar.moveTo(0,-5);
		    this.healthbar.lineTo((RPG.gameLength-83) * this.getCurrentHealth() / this.getMaxHealth(), -5);
		    this.healthbar.endFill();
		}
	},
	interfaceUpdateExp: function() {
		if (this.getLevel() < 50) {
			if (this.getExpPercent() < 100) {
				this.expbar.clear();
				var x = this.getExpPercent();
		    	var color = this.rgbToHex((x > 50 ? 1-2*(x-50)/100.0 : 1.0) * 255, (x > 50 ? 1.0 : 2*x/100.0) * 255, 0);

		    	this.expbar.beginFill(color);
		    	this.expbar.lineStyle(5, color, 1);
		    	this.expbar.moveTo(0,-5);
		    	this.expbar.lineTo((RPG.gameLength-83) * this.getExp() / this.getCurrentMaxExp(), -5);
		    	this.expbar.endFill();
			}
			else if (this.getExpPercent() >= 100) {
				this.expbar.clear();
				this.incrementLevel();
				this.expText.text = "Level " + this.getLevel();
				this.exp = 0;
			}
		}
		else {
			this.expbar.clear();
			this.expText.text = "Level 50 @ " + this.getExp() + " exp";
		}
	},
	interfaceDisplayProfile: function() {
		if (this.isSkillsDisplayed) {
			console.log("Skills menu destroyed");
			this.isSkillsDisplayed = false;
		}
		if (!this.isProfileDisplayed) {
			this.profileGroup = this.game.add.group();
			this.profileGroup.z = 5;
			this.profileGroup.fixedToCamera = true;
			this.profileMenu = this.game.add.image(0, 0, 'profile');
			this.profileGroup.add(this.profileMenu);
			this.exit_profil = this.game.add.button(430, 500, 'exit', function() {
				this.profileGroup.destroy(true);
				this.isProfileDisplayed = false;
			}, this);
			this.profileGroup.add(this.exit_profil);
			var font = {font: "22px Roboto Slab", align: "center", stroke: "#000000"};
			// strength
			this.profStrenTxt = this.game.add.text(290, 215, String(this.strength), font);
			var grd = this.profStrenTxt.context.createLinearGradient(0, 0, 0, this.profStrenTxt.canvas.height);
    		grd.addColorStop(0, '#F9F9F9');   
			grd.addColorStop(1, '#C7C7C7');
			this.profStrenTxt.fill = grd;
			this.profileGroup.add(this.profStrenTxt);
			// int
			this.profIntelTxt = this.game.add.text(290, 287, String(this.intelligence), font);
			var grd = this.profIntelTxt.context.createLinearGradient(0, 0, 0, this.profIntelTxt.canvas.height);
    		grd.addColorStop(0, '#F9F9F9');   
			grd.addColorStop(1, '#C7C7C7');
			this.profIntelTxt.fill = grd;
			this.profileGroup.add(this.profIntelTxt);
			// dext
			this.profDextTxt = this.game.add.text(290, 359, String(this.dexterity), font);
			var grd = this.profDextTxt.context.createLinearGradient(0, 0, 0, this.profDextTxt.canvas.height);
    		grd.addColorStop(0, '#F9F9F9');   
			grd.addColorStop(1, '#C7C7C7');
			this.profDextTxt.fill = grd;
			this.profileGroup.add(this.profDextTxt);
			// stamina
			this.profStamTxt = this.game.add.text(290, 431, String(this.stamina), font);
			var grd = this.profStamTxt.context.createLinearGradient(0, 0, 0, this.profStamTxt.canvas.height);
    		grd.addColorStop(0, '#F9F9F9');   
			grd.addColorStop(1, '#C7C7C7');
			this.profStamTxt.fill = grd;
			this.profileGroup.add(this.profStamTxt);
			this.isProfileDisplayed = true;
		}
		else {
			this.profileGroup.destroy(true);
			this.isProfileDisplayed = false;
		}
	},
	interfaceDisplaySkills: function() {
		if (this.isProfileDisplayed) {
			this.profileGroup.destroy(true);
			this.isProfileDisplayed = false;
		}
		if (!this.isSkillsDisplayed) {
			// this.skillsMenu = this.game.add.image(400, 75, 'skills');
			// ajouter pour chaque ability un mini-menu quand on passe la souris dessus
			// contenant dégâts de base, nom, et description ainsi que l'icône du sort et son raccourci
			console.log("Skills menu displayed");
			this.isSkillsDisplayed = true;
		}
		else {
			// this.skillsMenu.destroy(true);
			console.log("Skills menu destroyed");
			this.isSkillsDisplayed = false;
		}
	},
	interfaceDisplaySave: function() {
		console.log('je sauvegarde');
		var player_to_save = {
			id : this.getId(), 
			session : this.getSession(),
			name : this.getName(),
			job : this.getJob(),
			level : this.getLevel(),
			exp : this.getExp(),
			expPercent : this.getExpPercent(),
			strength : this.getStrength(),
			dexterity : this.getDexterity(),
			intelligence : this.getIntelligence(),
			stamina : this.getStamina(),
			maxHealth : this.getMaxHealth(),
			currentHealth : this.getCurrentHealth(),
			x : this.sprite.x,
			y : this.sprite.y,
			map : this.getMap(),
		  	orientation : this.getOrientation(),
		  	direction : this.getDirection()
		};
		RPG.socket.emit('send_character',JSON.stringify(player_to_save));
	},
	interfaceDisplayQuit: function() {
		// save before quitting
		this.interfaceDisplaySave();
		document.location.reload(true);
	},
	rgbToHex: function(r, g, b) {
    	return "0x" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	},
	isAlive: function() {
		if (this.getCurrentHealth() <= 0) {
			var font1 = {font: "35px Roboto Slab", align: "center", stroke: "#000000"};
			var font2 = {font: "22px Roboto Slab", align: "center", stroke: "#000000"};
			this.alive = false;
			this.deathCount++;
			var str = 'You are dead!\nDeath count: ' + this.deathCount;
			var text1 = this.game.add.text((RPG.gameLength/2)-100, (RPG.gameWidth/2)-75, str, font1);
			var grd = text1.context.createLinearGradient(0, 0, 0, text1.canvas.height);
    		grd.addColorStop(0, '#DAFFF9');   
			grd.addColorStop(1, '#829995');
			text1.fill = grd;
			var text2 = this.game.add.text((RPG.gameLength/2)-40, (RPG.gameWidth/2)+10, 'Play again', font2);
			grd = text2.context.createLinearGradient(0, 0, 0, text2.canvas.height);
    		grd.addColorStop(0, '#DAFFF9');   
			grd.addColorStop(1, '#829995');
			text2.fill = grd;
			text2.inputEnabled = true;
			text2.events.onInputDown.add(this.dead, this);
			text1.fixedToCamera = true;
			text2.fixedToCamera = true;
			RPG.socket.emit('disconnect');
			this.sprite.reset();
		}
	},
	dead: function() {
		// save death count (only) in browser
		// so the player gets to last save with the death count increased
		document.location.reload(true);
	},
	reposition: function() {
	    this.sprite.x = this.tilex;
	    this.sprite.y = this.tiley;
	},
	movements:  function() {
	    this.sprite.body.velocity.x = 0;
	    this.sprite.body.velocity.y = 0;
	    if (this.alive) {
		    var speed = 100;
			if (this.cursors.left.isDown && this.cursors.up.isDown) {
				this.sprite.body.velocity.x = -speed;
				this.sprite.body.velocity.y = -speed;
				RPG.socket.emit('actualise_position_server',{x : this.sprite.x,y : this.sprite.y});
			}
			if (this.cursors.left.isDown && this.cursors.down.isDown) {
		        this.sprite.body.velocity.x = -speed;
		        this.sprite.body.velocity.y = +speed;
		        RPG.socket.emit('actualise_position_server',{x : this.sprite.x,y : this.sprite.y});
			}
			if (this.cursors.right.isDown && this.cursors.down.isDown) {
		        this.sprite.body.velocity.x = +speed;
		        this.sprite.body.velocity.y = +speed;
		        RPG.socket.emit('actualise_position_server',{x : this.sprite.x,y : this.sprite.y});
			}
			if (this.cursors.right.isDown && this.cursors.up.isDown) {
		        this.sprite.body.velocity.x = +speed;
		        this.sprite.body.velocity.y = -speed;
		        RPG.socket.emit('actualise_position_server',{x : this.sprite.x,y : this.sprite.y});
			}
			if (this.cursors.left.isDown && !(this.cursors.up.isDown) && !(this.cursors.down.isDown)) {
		        //  Move to the left
		        this.sprite.body.velocity.x = -speed;
		        this.orientation = 2;
		        this.sprite.animations.play('left');
		        RPG.socket.emit('actualise_position_server',{x : this.sprite.x,y : this.sprite.y});
	        	RPG.socket.emit('animation_server',{direction: 'left' ,orientation : 2});
		    }
		    else if (this.orientation == 2) {
				this.sprite.frame = 4;
				this.sprite.animations.stop();
				RPG.socket.emit('stop_animation',{nb_frame : 4});
		    }
		    if (this.cursors.right.isDown && !(this.cursors.up.isDown) && !(this.cursors.down.isDown)) {
		        //  Move to the right
		        this.sprite.body.velocity.x = speed;
		        this.orientation = 4;
		        this.sprite.animations.play('right');
		        RPG.socket.emit('actualise_position_server',{x : this.sprite.x,y : this.sprite.y});
	        	RPG.socket.emit('animation_server',{direction: 'right' ,orientation : 4});
		    }
		    else if (this.orientation == 4)
		    {
				this.sprite.frame = 8;
				this.sprite.animations.stop();
				RPG.socket.emit('stop_animation',{nb_frame : 8});
		    }
		    if (this.cursors.up.isDown && !(this.cursors.left.isDown) && !(this.cursors.right.isDown))
		    {
		        //  Move up
		        this.sprite.body.velocity.y = -speed;
				this.orientation = 1;
		        this.sprite.animations.play('up');
		        RPG.socket.emit('actualise_position_server',{x : this.sprite.x,y : this.sprite.y});
	        	RPG.socket.emit('animation_server',{direction : 'up',orientation : 1});
		    }
		    else if (this.orientation == 1)
		    {
				this.sprite.frame = 12;
				this.sprite.animations.stop();
				RPG.socket.emit('stop_animation',{nb_frame : 12});
		    }
			if (this.cursors.down.isDown && !(this.cursors.left.isDown) && !(this.cursors.right.isDown)) {
			    //  Move down
				this.orientation = 3;
				this.sprite.body.velocity.y = speed;
				this.sprite.animations.play('down');
				RPG.socket.emit('actualise_position_server',{x : this.sprite.x,y : this.sprite.y});
				RPG.socket.emit('animation_server',{direction : 'down',orientation : 3});
			}
			else if (this.orientation == 3)
			{
				this.sprite.frame = 0;
				this.sprite.animations.stop();
				RPG.socket.emit('stop_animation',{nb_frame : 0});
		    }
		}
	},
	// sets name of character
	setName: function(name) {
		if (typeof name === "string") {
			this.name = name;
		}
	},
	// gets the name of character
	getName: function() {
		return this.name;
	},
	setSession: function(session) {
		this.session = session;
	},
	getSession: function() {
		return this.session;
	},
	setMovable : function(movable){
		this.movable = movable;
	},
	getMovable : function(){
		return this.movable;

	},
	// sets id of the character
	setId: function(id){
		this.id = id;
	},
	// gets the id of the character
	getId: function(){
		return this.id;
	},
	setExp: function(exp){
		this.exp = exp;
	},
	getExp: function(){
		return this.exp;
	},
	setStrength: function(strength){
		this.strength = strength;
	},
	getStrength: function(){
		return this.strength;
	},
	setIntelligence: function(intelligence){
		this.intelligence = intelligence;
	},
	getIntelligence: function(){
		return this.intelligence;
	},
	setStamina: function(stamina){
		this.stamina = stamina;
	},
	getStamina: function(){
		return this.stamina;
	},
	setDexterity: function(dexterity){
		this.dexterity = dexterity;
	},
	getDexterity: function(){
		return this.dexterity;
	},
	setExpPercent: function(expPercent){
		this.expPercent = expPercent;
	},
	setMap: function(map){
		this.map = map;
	},
	getMap: function(){
		return this.map;
	},
	setOrientation : function(orientation){
		this.orientation = orientation;
	},
	getOrientation : function(){
		return this.orientation;
	},
	setDirection : function(direction){
		this.sprite.direction = direction;
	},
	getDirection : function(){
		return this.sprite.direction;
	},
	// sets job of character
	setJob: function(job) {
		if (job == "Berseker" || job == "Neogician") {
			this.job = job;
		}
	},
	// gets the job of character
	getJob: function() {
		return this.job;
	},
	// gets max health of character
	getMaxHealth: function() {
		return this.maxHealth;
	},
	// changes max health of character
	setMaxHealth: function(hp) {
		if (hp > 0) {
			this.maxHealth = hp;
		}
	},
	// gets current health
	getCurrentHealth: function() {
		return this.currentHealth;
	},
	// changes current health
	setCurrentHealth: function(hp) {
		if (hp >= 0) {
			this.currentHealth = hp;
			RPG.socket.emit('set_characteristics',{type : 'currentHealth', value : this.getCurrentHealth() });
		}
	},
	// decreases or increases current health (damage taken or natural regen for example)
	modifyCurrentHealth: function(hp) {
		if (hp >= this.getMaxHealth()-this.getCurrentHealth()) {
			this.setCurrentHealth(this.getMaxHealth());
		}
		else if (hp < 0 && hp*(-1) > this.getCurrentHealth()) {
			this.setCurrentHealth(0);
		}
		else {
			this.setCurrentHealth(this.getCurrentHealth() + hp);
		}
	},
	// sets charac value
	setCharacValue: function(characteristic, value) {
		if (value >= 0) {
			switch (characteristic) {
				case "Strength":
					this.strength = value;
					RPG.socket.emit('set_characteristics',{type : 'strength', value : this.strength});
					break;
				case "Dexterity":
					this.dexterity = value;
					RPG.socket.emit('set_characteristics',{type : 'dexterity', value : this.dexterity});
					break;
				case "Intelligence":
					this.intelligence = value;
					RPG.socket.emit('set_characteristics',{type : 'intelligence', value : this.intelligence});
					break;
				case "Stamina":
					this.stamina = value;
					RPG.socket.emit('set_characteristics',{type : 'stamina', value : this.stamina});
					break;
			}
		}
	},
	// adds 1 to a specific characteristic
	incrementCharac: function(characteristic) {
		switch (characteristic) {
			case "Strength":
				this.strength++;
				RPG.socket.emit('set_characteristics',{type : 'strength', value : this.strength});
				break;
			case "Dexterity":
				this.dexterity++;
				RPG.socket.emit('set_characteristics',{type : 'dexterity', value : this.dexterity});
				break;
			case "Intelligence":
				this.intelligence++;
				RPG.socket.emit('set_characteristics',{type : 'intelligence', value : this.intelligence});
				break;
			case "Stamina":
				this.stamina++;
				RPG.socket.emit('set_characteristics',{type : 'stamina', value : this.stamina});
				break;
		}
	},
	// gets value of a specific characteristic
	getCharacValue: function(characteristic, value) {
		switch (characteristic) {
			case "Strength":
				return this.strength;
				break;
			case "Dexterity":
				return this.dexterity;
				break;
			case "Intelligence":
				return this.intelligence;
				break;
			case "Stamina":
				return this.stamina;
				break;
		}
		// error case
		return -1;
	},
	// changes move speed
	setMoveSpeed: function(ms) {
		if (ms >= 0) {
			this.moveSpeed = ms;
		}
	},
	// returns move speed
	getMoveSpeed: function() {
		return this.moveSpeed;
	},
	// sets level of character
	setLevel: function(lvl) {
		if (lvl > 0 && lvl <= 50) {
			this.level = lvl;
			RPG.socket.emit('set_characteristics',{type : 'level', value : this.getLevel() });
		}
	},
	// returns level of character
	getLevel: function() {
		return this.level;
	},
	// increments exp
	incrementsExp: function(xp) {
		this.exp += xp;
		RPG.socket.emit('set_characteristics',{type : 'exp', value : this.getExp() });
	},
	// gets exp
	getExp: function() {
		return this.exp;
	},
	getExpPercent: function() {
		return parseInt((this.getExp()/this.getCurrentMaxExp())*100);
	},
	getCurrentMaxExp: function() {
		return (this.getLevel())*Math.pow(this.getLevel(),2);
	},
	// adds 1 to character's level
	incrementLevel: function() {
		if (this.level < 50) {
			this.setLevel(this.getLevel()+1);
			this.growStats();
			if (this.levelUpCount == 0) {
				this.displayUpCharac();
			}
			this.levelUpCount++;
		}
	},
	// increments stats on leveling up depending of job
	growStats: function() {
		this.upMaxHealth();
		if (this.job == "Neogician") {
			this.incrementCharac("Intelligence");
			this.incrementCharac("Intelligence");
		}
		else if (this.job == "Mechsoldier") {
			this.incrementCharac("Dexterity");
			this.incrementCharac("Strength");
		}
	},
	// changes max health on level up
	upMaxHealth: function() {
		this.setMaxHealth(this.getMaxHealth()+20);
		this.setCurrentHealth(this.getMaxHealth());
	},
	interfaceAddUpText: function() {
		var font = {font: "15px Roboto Slab", align: "center", stroke: "#000000"};
	  	this.upText = this.game.add.text(10, 10, '', font);
	  	this.upText.fixedToCamera = true;
	  	var grd = this.upText.context.createLinearGradient(0, 0, 0, this.upText.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		this.upText.fill = grd;
	  	// strength
	  	this.strengthText = this.game.add.text(10, 55, '', font);
		this.strengthText.inputEnabled = true;
		this.strengthText.fixedToCamera = true;
		this.strengthText.events.onInputDown.add(this.upStrength, this);
		grd = this.strengthText.context.createLinearGradient(0, 0, 0, this.strengthText.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		this.strengthText.fill = grd;
		// intelligence
		this.intelligenceText = this.game.add.text(100, 55, '', font);
		this.intelligenceText.inputEnabled = true;
		this.intelligenceText.fixedToCamera = true;
		this.intelligenceText.events.onInputDown.add(this.upIntelligence, this);
		grd = this.intelligenceText.context.createLinearGradient(0, 0, 0, this.intelligenceText.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		this.intelligenceText.fill = grd;
		// dexterity
		this.dexterityText = this.game.add.text(205, 55, '', font);
		this.dexterityText.inputEnabled = true;
		this.dexterityText.fixedToCamera = true;
		this.dexterityText.events.onInputDown.add(this.upDexterity, this);
		grd = this.dexterityText.context.createLinearGradient(0, 0, 0, this.dexterityText.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		this.dexterityText.fill = grd;
		// stamina
		this.staminaText = this.game.add.text(300, 55, '', font);
		this.staminaText.inputEnabled = true;
		this.staminaText.fixedToCamera = true;
		this.staminaText.events.onInputDown.add(this.upStamina, this);
		grd = this.staminaText.context.createLinearGradient(0, 0, 0, this.staminaText.canvas.height);
    	grd.addColorStop(0, '#DAFFF9');   
		grd.addColorStop(1, '#829995');
		this.staminaText.fill = grd;
	},
	// changes characs on level up
	displayUpCharac: function() {
		if (this.levelUpCount >= 0) {
			this.upText.setText("Level up!\nYou can upgrade one of your characteristics!");
			this.strengthText.setText("Strength (U)");
			this.intelligenceText.setText("Intelligence (I)");
			this.dexterityText.setText("Dexterity (O)");
			this.staminaText.setText("Stamina (P)");
		}
	},
	destroyUpText: function() {
		this.levelUpCount--;
		console.log("dext: " + this.dexterity);
		console.log("stren: " + this.strength);
		console.log("intell: " + this.intelligence);
		console.log("stamina: " + this.stamina);
		if (this.levelUpCount <= 0) {
			this.strengthText.setText("");
			this.intelligenceText.setText("");
			this.dexterityText.setText("");
			this.staminaText.setText("");
			this.upText.setText("");
		}
	},
	// up strength on level up
	upStrength: function() {
		if (this.levelUpCount > 0) {
			this.incrementCharac("Strength");
			this.destroyUpText();
		}
	},
	// up intelligence on level up
	upIntelligence: function() {
		if (this.levelUpCount > 0) {
			this.incrementCharac("Intelligence");
			this.destroyUpText();
		}
	},
	// up dexterity on level up
	upDexterity: function() {
		if (this.levelUpCount > 0) {
			this.incrementCharac("Dexterity");
			this.fireRate = this.setAttackSpeed()*1000;
			this.destroyUpText();
		}
	},
	// up stamina on level up
	upStamina: function() {
		if (this.levelUpCount > 0) {
			this.incrementCharac("Stamina");
			this.setMaxHealth(this.getMaxHealth()+5);
			this.destroyUpText();
		}
	},
	setAbilities : function(abilities) {
		this.abilities = abilities;
	},
	getAbilities : function() {
		return this.abilities;
	},
	// fires
	fire : function(bullets) {
		if (this.action.isDown)
		{
	  		if (this.game.time.now > this.nextFire && bullets.countDead() > 0)
	    	{
	    		this.baseDamages['min'] = this.strength + 1;
	    		this.baseDamages['max'] = this.strength + 1;
	    		this.nextFire = this.game.time.now + this.fireRate;
				var bullet = bullets.getFirstExists(false);
				bullet.lifespan = 2000;
				this.game.physics.arcade.enable(bullet);
		        switch(true)
		        {
		        	case (this.orientation == 1): 
		        	    bullet.reset(this.sprite.x, this.sprite.y-18);
		        		bullet.rotation = (-Math.PI)/2; 
		        		bullet.body.velocity.y = -300;
		        		break;
		        	case (this.orientation == 2): 
		        	    bullet.reset(this.sprite.x-8, this.sprite.y);
		        		bullet.rotation = (-Math.PI); 
		        		bullet.body.velocity.x = -300;
		        		break;
		        	case (this.orientation == 3): 
		        	    bullet.reset(this.sprite.x, this.sprite.y+18);
		        		bullet.rotation = (Math.PI)/2; 
		        		bullet.body.velocity.y = 300;
		        		break;
		        	case (this.orientation == 4): 
		        	    bullet.reset(this.sprite.x+8, this.sprite.y);
		        	    bullet.rotation = 0; 
		        		bullet.body.velocity.x = 300;
		        		break;
		        }
	    	}
	    }
	},
	scatterFire : function(bullets) {
	  	if (this.game.time.now > this.spell4NextFire && bullets.countDead() > 0)
	    {
	    	this.baseDamages['min'] = this.abilities[3].getBaseMinDamage();
	    	this.baseDamages['max'] = this.abilities[3].getBaseMaxDamage();
	    	this.spell4NextFire = this.game.time.now + 20;
			var bullet = bullets.getFirstExists(false);
			bullet.lifespan = 3000;
			this.game.physics.arcade.enable(bullet);
		    switch(true)
		    {
		       	case (this.orientation == 1): 
		       	    bullet.reset(this.sprite.x + this.game.rnd.between(-10, 10), this.sprite.y-18);
		       		bullet.rotation = (-Math.PI)/2; 
		       		bullet.body.velocity.y = -300;
		       		break;
		       	case (this.orientation == 2): 
		       	    bullet.reset(this.sprite.x-8, this.sprite.y + this.game.rnd.between(-10, 10));
		       		bullet.rotation = (-Math.PI); 
		       		bullet.body.velocity.x = -300 ;
		       		break;
		       	case (this.orientation == 3): 
		       	    bullet.reset(this.sprite.x + this.game.rnd.between(-10, 10), this.sprite.y+18);
		       		bullet.rotation = (Math.PI)/2; 
		       		bullet.body.velocity.y = 300;
		       		break;
		       	case (this.orientation == 4): 
		       	    bullet.reset(this.sprite.x+8, this.sprite.y + this.game.rnd.between(-10, 10));
		       	    bullet.rotation = 0; 
		       		bullet.body.velocity.x = 300;
		       		break;
		    }
		    if (this.game.time.now > this.spell4ActivationTime + this.spell4Now)
		    {
		    	this.spell4Activation = false;
		    }
	    }
	},
	shieldUpdate : function()
	{
		if (this.game.time.now > this.spell5ActivationTime + this.spell5Now)
		{
			this.spell5Activation = false;
			this.shield.animations.stop();
			this.shield.animations.add('energy_shield_closed', [13,14,15,16,17,18,19,20]);
			this.shield.animations.play('energy_shield_closed', 10, false, true)
			this.stamina = this.baseStamina;
		}
	} ,

	shieldCreate : function ()
	{
		this.shield = this.game.add.sprite(-98 , -80 , 'shield');
	    this.sprite.addChild(this.shield);
	    this.shield.animations.add('energy_shield_activated' , [ 9 , 10 , 11 , 10]);
    	this.shield.animations.play('energy_shield_activated' , 25, true , true);
    	this.game.physics.enable(this.shield, Phaser.Physics.ARCADE);
    	this.baseStamina = this.stamina;
    	this.stamina += 30;
	} , 

	// deals damages based on characteristics
	damageDealt: function(type) {
		if (type === "Physical") {
			var minDealt = this.baseDamages['min']*(1+(0.075*this.strength));
			var maxDealt = this.baseDamages['max']*(1+(0.075*this.strength));
			var damage = Math.round(Math.random()*(maxDealt - minDealt)+minDealt);
			console.log('physical damage dealt: ' + damage);
			return damage;
		}
		else if (type === "Magical") {
			var minDealt = this.baseDamages['min']*(1+(0.05*this.intelligence));
			var maxDealt = this.baseDamages['max']*(1+(0.05*this.intelligence));
			var damage = Math.round(Math.random()*(maxDealt - minDealt)+minDealt);
			console.log('magic damage dealt: ' + damage);
			return damage;
		}
	},

	damageReceived: function(dmg) {
		var dmgReceived = Math.round(dmg/(1+this.stamina*0.05));
		console.log('damage received by player: ' + dmgReceived);
		this.modifyCurrentHealth(-1*dmgReceived);
		return dmgReceived;
	},

	setAttackSpeed: function() {
		return 1-this.dexterity*0.015;
	},

};