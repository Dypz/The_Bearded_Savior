var Monster = function(start_x, start_y,start_range,start_hp,start_level,start_exp,start_orientation,start_direction,start_map,start_damage,start_sprite_number){
	var x = start_x,
		y = start_y,
		range = start_range,
		hp = start_hp,
		level = start_level,
		exp = start_exp,
		orientation = start_orientation,
		direction = start_direction,
		map = start_map,
		damage = start_damage,
		sprite_number = start_sprite_number,
		id;

	var getX = function(){
		return x;
	};

	var getY = function(){
		return y;
	};

	var setX = function(new_x){
		x = new_x;
	};

	var setY = function(new_y){
		y = new_y;
	};

	var getRange = function(){
		return range;
	};

	var setRange = function(new_range){
		range = new_range;
	};

	var setHp = function(new_hp){
		hp = new_hp;
	};

	var getHp = function(){
		return hp;
	};

	var getLevel = function(){
		return level;
	};

	var setLevel = function(new_level){
		level = new_level;
	};

	var getExp = function(){
		return exp;
	};

	var setExp = function(new_exp){
		exp = new_exp;
	};

	var getOrientation = function(){
		return orientation;
	};

	var setOrientation = function(new_orientation){
		orientation = new_orientation;
	};

	var getDirection = function(){
		return direction;
	};

	var setDirection = function(new_direction){
		direction = new_direction;
	};

	var getMap = function(){
		return map;
	};

	var setMap = function(new_map){
		map = new_map;
	};

	var getDamage = function(){
		return damage;
	};

	var setDamage = function(new_damage){
		damage = new_damage;
	};

	var getSpriteNumber = function(){
		return sprite_number;
	};

	var setSpriteNumber = function(new_sprite_number){
		sprite_number = new_sprite_number;
	};

	return {
		getX: getX,
		getY: getY,
		setX: setX,
		setY: setY,
		getRange : getRange,
		setRange: setRange,
		getHp : getHp,
		setHp : setHp,
		setLevel : setLevel,
		getLevel : getLevel,
		setExp : setExp,
		getExp : getExp,
		getOrientation: getOrientation,
		setOrientation: setOrientation,
		getDirection: getDirection,
		setDirection: setDirection,
		setMap: setMap,
		getMap: getMap,
		setDamage: setDamage,
		getDamage : getDamage,
		setSpriteNumber: setSpriteNumber,	
		getSpriteNumber: getSpriteNumber,
		id: id
	}
};
exports.Monster = Monster;