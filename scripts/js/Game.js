RPG.Game = function(game) {
	this.game = game;
	RPG.socket = io.connect('http://localhost:8080');
	RPG.game = this.game;
	RPG.pseudo;

	RPG.users_id = new Array();
	RPG.users = new Array();

	RPG.monsters_id = new Array();
	RPG.monsters = new Array();
	RPG.move_in_monsters = 0;

	RPG.current_map;

	RPG.id;
	RPG.received_monster_id = false;
	RPG.monster_id_to_fix = new Array();
};

RPG.Game.prototype = {
	preload: function() {  	
    	//Création d'une instance pour le joueur
    	this.player = new Character(this.game);
    	this.player.preload();
    	if(RPG.selected == null)
    	{
    		RPG.pseudo = sessionStorage.getItem("pseudo");
    	}
    	else
    	{
    		RPG.pseudo = RPG.selected.name;
    	}

	    this.tileSize = 64;
		this.dRows = 30;
		this.dCols = 26;

		this.monstern = -1;

    	//Création d'une instance pour les this.dialogues
    	this.dialogue = new Dialogue(this.game);
    	this.dialogue.preload();

		//créons un NPC
		this.npcT=new Array();
		this.npcn=-1;
	} ,

	create: function() {
		this.stage.disableVisibilityChange = true;

		this.isSpeaking = false;
		this.textOk = false;
  		this.actualText = "";
  		this.lineNumber = 1;
  		this.currentScene = 0;
  		this.dialogWait = false;
  		this.index = 0;
		this.talk = this.game.input.keyboard.addKey(Phaser.Keyboard.E);

		this.content = ["Toc toc*Putain mais qui peut bien venir me les briser*aussi tôt il est même pas 15h00!!" ];

		if(RPG.selected == null)
		{
			this.player.create(100,100,RPG.choosen_job,RPG.pseudo,1,0,0,this.player.getMaxHealth(),0,0,0,0,'level1','left',4,true);
			RPG.current_map = 'level1';
			this.loadMap( RPG.current_map , 'chambre','chambre' , 'chambre','chambre' , 500 , 400 );
			RPG.socket.emit('get_connected_players',RPG.current_map);
		}
		else
		{
			this.player.create(RPG.selected.x,RPG.selected.y,RPG.selected.job,RPG.pseudo,RPG.selected.level,RPG.selected.exp,RPG.selected.expPercent,RPG.selected.currentHealth,RPG.selected.strength,RPG.selected.intelligence,RPG.selected.stamina,RPG.selected.dexterity,RPG.selected.map,RPG.selected.direction,RPG.selected.orientation,true);
			switch(RPG.selected.map){
				case 'level1' :
					RPG.current_map = 'level1';
					this.loadMap( RPG.current_map , 'chambre','chambre' , 'chambre','chambre' , RPG.selected.x , RPG.selected.y );
					break;
				case 'level2' :
					RPG.current_map = 'level2';
					this.loadMap( 'level2' , 'batiment1','rue' , 'batiment','rue' , RPG.selected.x ,RPG.selected.y);
					break;
				case 'level3' :
					RPG.current_map = 'level3';
					this.loadMap( 'level3' , 'batiment1','rue' , 'batiment','rue' , RPG.selected.x ,RPG.selected.y);
					break;
				case 'level4' :
					RPG.current_map = 'level4';
					this.loadMap( 'level4' , 'batiment1','rue' , 'batiment','rue' , RPG.selected.x ,RPG.selected.y);
					break;
				case 'level5' :
					RPG.current_map = 'level5';
					this.loadMap( 'level5' , 'batiment1','rue' , 'batiment','rue' ,RPG.selected.x ,RPG.selected.y);
					break;
				case 'level6' :
					RPG.current_map = 'level6';
					this.loadMap( 'level6' , 'batiment1','rue' , 'batiment','rue' , RPG.selected.x ,RPG.selected.y);
					break;
			};
		}

		//For player fire
		this.bullets;
		this.spell1;
		this.spell3;
		this.inv; 
		this.fireRate = 750;
		this.nextFire = 0;
		
		//	Enable p2 physics
		this.game.physics.startSystem(Phaser.Physics.P2JS);

		this.music = this.game.add.audio('nightwish');
    	this.music.play();

		//  Make things a bit more bouncey
	    this.game.physics.p2.defaultRestitution = 0.8;
	    this.dialogue.create();
	    //this.game.time.events.add(500, function () {this.createTextbox();}, this);
	},

	update: function()
	{
		var self = this;
	    RPG.socket.on('add_player',function(data){
	    	if(data.map == RPG.current_map){
	    		var index = RPG.users_id.indexOf(data.id);
	    		if(index == -1) // on ajoute le sprite seulement s'il n'a pas déjà été créé
	    		{
		    		RPG.users_id.push(data.id);
					var expPercent = parseInt((data.exp/((data.level)*Math.pow(data.level,2))*100));
					RPG.users.push(new Character(self.game,data.x,data.y,data.job,data.name ,data.level,data.exp,expPercent,data.currentHealth,data.strength,data.intelligence,data.stamina,data.dexterity,data.map,data.direction,data.orientation,false ));
		    		var k = (RPG.users.length)-1;
		    		RPG.users[k].create(data.x,data.y,data.job,data.name ,data.level,data.exp,expPercent,data.currentHealth,data.strength,data.intelligence,data.stamina,data.dexterity,data.map,data.direction,data.orientation,false );
		    		RPG.users[k].setId(data.id);
	    		}
	    	}
	    });

		self = this;
	    RPG.socket.on('add_monster',function(data){
	    	if(data.map == RPG.current_map){
	    		var index = RPG.monsters_id.indexOf(data.id);
	    		if(index == -1) // on ajoute le sprite seulement s'il n'a pas déjà été créé
	    		{
	    			console.log('add monstre update : '+data.id);
		    		RPG.monsters_id.push(data.id);
		    		RPG.monsters.push(new Monster(self.game,data.sprite_number,data.x,data.y));
		    		var k = (RPG.monsters.length)-1;
		    		RPG.monsters[k].create(data.range ,data.hp , data.level, data.exp,RPG.current_map,data.damage, false);
		    		RPG.monsters[k].setId(data.id);
	    		}
	    	}
	    });
	    
	    if (this.dialogWait == true)
    	{
			if (this.isSpeaking == true && this.talk.isDown)
			{
				this.delay(500);
				this.newPage();
				//this.game.time.events.add(50, this.newPage , this);
			}
			else if (this.isSpeaking == false && this.talk.isDown)
			{
				this.delay(500);
				this.endDialog();
				this.delay(500);
			}

    	}
    	else if (this.isSpeaking == true && this.dialogWait == false)
    	{
    		this.game.time.events.add(5, function () {this.writeLetter();}, this);
    	}

		for (i = 0 ; i < RPG.monsters.length ; i++)
		{
			this.physics.arcade.collide(RPG.monsters[i].sprite, this.blockedLayer);
			this.physics.arcade.collide(this.player.sprite, RPG.monsters[i].sprite);
			this.physics.arcade.collide(this.bullets , RPG.monsters[i].sprite , function(sprite1, sprite2) {
				this.bulletHitMonster(sprite1, sprite2, 'bullets');
			}, null , this);
		}

		for (i = 0 ; i < RPG.monsters.length ; i++) {
			this.physics.arcade.collide(this.spell1, RPG.monsters[i].sprite, function(sprite1, sprite2) {
				this.bulletHitMonster(sprite1, sprite2, 'laser');
			}, null, this);
		}

		for (i = 0 ; i < RPG.monsters.length ; i++) {
			this.physics.arcade.overlap(this.spell3, RPG.monsters[i].sprite, function(sprite1, sprite2) {
				this.bulletHitMonster(sprite1, sprite2, 'fire');
			}, null, this);
		}

		for (i = 0 ; i < RPG.monsters.length ; i++) {
			this.physics.arcade.overlap(this.spell4, RPG.monsters[i].sprite, function(sprite1, sprite2) {
				this.bulletHitMonster(sprite1, sprite2, 'bullets');
			}, null, this);
		}

		for (i = 0 ; i< RPG.monsters.length ; i++) {
			this.physics.arcade.overlap(this.player.spell6Sprite, RPG.monsters[i].sprite, function(sprite1, sprite2) {
				this.bulletHitMonster(sprite2, sprite1, 'slash');
			}, null, this);
		}

		for (i = 0 ; i <= this.npcn ; i++) {
		this.physics.arcade.collide(this.player.sprite, this.npcT[i].sprite);
		}

		this.physics.arcade.collide(this.monstreLayer, this.monstreLayer);
		this.physics.arcade.collide(this.player.sprite, this.ennemy_bullets, this.bulletHitPlayer , null , this);
		for(i=0;i<RPG.monsters.length;i++)
		{
			for(j=0;j<RPG.users.length;j++)
			{
				this.physics.arcade.collide(RPG.users[j], RPG.monsters[i].sprite);
				//this.physics.arcade.collide(RPG.users[j].sprite, this.ennemy_bullets, this.bulletHitPlayer , null , this);
				this.physics.arcade.collide(RPG.users[j].sprite, this.blockedLayer);
				this.physics.arcade.overlap(RPG.users[j].spell6Sprite, RPG.monsters[i].sprite, function(sprite1, sprite2) {
					this.bulletHitMonster(sprite2, sprite1, 'slash');
				}, null, this);
			}
		}

		this.physics.arcade.collide(this.player.sprite, this.blockedLayer);
		this.physics.arcade.collide(this.bullets , this.blockedLayer , this.bulletHitWall , null, this);
		this.physics.arcade.collide(this.spell1, this.blockedLayer , this.bulletHitWall , null, this);
		this.physics.arcade.collide(this.spell4, this.blockedLayer , this.bulletHitWall , null, this);

		this.physics.arcade.collide(this.spell1, this.spriteLayer , this.bulletHitWall , null, this);
		this.physics.arcade.collide(this.spell4, this.spriteLayer , this.bulletHitWall , null, this);

		this.physics.arcade.collide(this.bullets , this.spriteLayer , this.bulletHitWall , null, this);
		this.physics.arcade.collide(this.ennemy_bullets , this.spriteLayer , this.bulletHitWall , null, this);

		this.physics.arcade.collide(this.ennemy_bullets, this.blockedLayer , this.bulletHitWall , null, this);

		for (i = 0 ; i <= this.npcn ; i++) {
		this.npcT[i].update( this.player , this.game);
		}

		this.player.update(this.bullets, this.spell1, this.spell3);

		if (this.boss)
		{


				//Boss collide
			this.physics.arcade.collide(this.player.sprite, this.boss.bullets1 , function (sprite1 , sprite2){this.bossBulletHitPlayer(sprite1 , sprite2 , 1);} , null , this);
			this.physics.arcade.collide(this.boss.bullets1 , this.spriteLayer , this.bulletHitWall , null , this);
			this.physics.arcade.collide(this.boss.bullets1 , this.blockedLayer , this.bulletHitWall , null , this);

			this.physics.arcade.collide(this.player.sprite, this.boss.bullets2 , this.bulletHitPlayer , null , this);
			this.physics.arcade.collide(this.boss.bullets2 , this.spriteLayer , this.bulletHitWall , null , this);
			this.physics.arcade.collide(this.boss.bullets2 , this.blockedLayer , this.bulletHitWall , null , this);

			this.physics.arcade.collide(this.player.sprite, this.boss.bullets3 , function (sprite1 , sprite2){this.bossBulletHitPlayer(sprite1 , sprite2 , 2);} , null , this);
			this.physics.arcade.collide(this.boss.bullets3 , this.spriteLayer , this.bulletHitWall , null , this);
			this.physics.arcade.collide(this.boss.bullets3 , this.blockedLayer , this.bulletHitWall , null , this);

			this.physics.arcade.collide(this.player.sprite, this.boss.bullets4 , this.bulletHitPlayer , null , this);
			this.physics.arcade.collide(this.boss.bullets4 , this.spriteLayer , this.bulletHitWall , null , this);
			this.physics.arcade.collide(this.boss.bullets4 , this.blockedLayer , this.bulletHitWall , null , this);

			this.physics.arcade.collide(this.player.sprite, this.boss.bullets5 , function (sprite1 , sprite2){this.bossBulletHitPlayer(sprite1 , sprite2 , 3);} , null , this);
			this.physics.arcade.collide(this.boss.bullets5 , this.spriteLayer , this.bulletHitWall , null , this);
			this.physics.arcade.collide(this.boss.bullets5 , this.blockedLayer , this.bulletHitWall , null , this);

			this.physics.arcade.collide(this.player.sprite, this.boss.sprite , function () {this.boss.closeContact(this.player);
			this.createDmgText(10, this.player.sprite, "Ally");}  , null , this);

			this.physics.arcade.collide(this.boss.sprite, this.blockedLayer);


			this.physics.arcade.collide(this.bullets , this.boss.sprite , function(sprite1, sprite2) {this.bulletHitMonster(sprite1, sprite2, 'bullets');} , null ,  this);
				this.physics.arcade.collide(this.spell1, this.boss.sprite, function(sprite1, sprite2) {
					this.bulletHitMonster(sprite1, sprite2, 'laser');
				}, null, this);
			this.physics.arcade.overlap(this.spell3, this.boss.sprite, function(sprite1, sprite2) {
					this.bulletHitMonster(sprite1, sprite2, 'fire');
				}, null, this);

				this.physics.arcade.overlap(this.spell4, this.boss.sprite, function(sprite1, sprite2) {
					this.bulletHitMonster(sprite1, sprite2, 'bullets');
				}, null, this);
				this.physics.arcade.overlap(this.player.spell6Sprite, this.boss.sprite, function(sprite1, sprite2) {
					this.bulletHitMonster(sprite2, sprite1, 'slash');
				}, null, this);

			this.boss.update(this.player );

		}

		for (i = 0 ; i < RPG.monsters.length ; i++) {
			RPG.monsters[i].update(this.player , this.ennemy_bullets );
		}

		this.checkMapTransition();
		this.player.updateInterface();

		/******************* Gestion des évènements **************************/


    	RPG.socket.on('send_new_position',function(data){
	    	var index = RPG.users_id.indexOf(data.id);
	    	if(index != -1 && data.map == RPG.current_map)
	    	{
	    		RPG.users[index].sprite.x = data.x;
		    	RPG.users[index].sprite.y = data.y;
	    	}
	    });

	    RPG.socket.on('send_new_animation',function(data){
	    	var index = RPG.users_id.indexOf(data.id);
	    	if(index != -1)
	    	{
	    		RPG.users[index].orientation = data.orientation;
	    		RPG.users[index].sprite.animations.play(data.direction);
	    	}
	    });

	    RPG.socket.on('send_stop_animation',function(data){
	    	var index = RPG.users_id.indexOf(data.id);
	    	if(index != -1)
	    	{
	    		RPG.users[index].sprite.animations.stop();
	    		RPG.users[index].frame = data.frame;
	    	}
	    });

	    RPG.socket.on('actualise_players_tab',function(id){
	    	var index = RPG.users_id.indexOf(id);
	    	if(index != -1)
	    	{
	    		RPG.users[index].sprite.kill();
	    		RPG.users.splice(index,1);
	    		RPG.users_id.splice(index,1);
	    	}
	    });

	    var self = this;
	    RPG.socket.on('play_spell',function(data){
	    	var index = RPG.users_id.indexOf(data.id);
	    	if(index != -1)
	    	{
	    		switch(data.type)
	    		{
	    			case 'spell1':
			    		RPG.users[index].animateSpell1(self.spell1);
		    			break;
		    		case 'spell2':
		    			RPG.users[index].animateSpell2();
		    			break;
		    		case 'spell3':
		    			RPG.users[index].animateSpell3(self.spell3);
		    			break;
		    		case 'spell4':
		    			RPG.users[index].animateSpell4(self.bullets);
		    			break;
		    		case 'spell5':
		    			RPG.users[index].animateSpell5();
		    			break;
		    		case 'spell6':
		    			RPG.users[index].animateSpell6();
		    			break;
	    		};
	    	}
	    });

	    /****************************************** Gestion monstres ***************************************/
	    
	    RPG.socket.on('send_new_monster_position',function(data){
	    	var index = RPG.monsters_id.indexOf(data.id);
	    	if(index != -1)
	    	{
		    	RPG.monsters[index].sprite.x = data.x;
		    	RPG.monsters[index].sprite.y = data.y;
	    	}
	    });	

	    RPG.socket.on('send_new_monster_animation',function(data){
	    	var index = RPG.monsters_id.indexOf(data.id);
	    	if(index != -1)
	    	{
		    	RPG.monsters[index].sprite.animations.play(data.direction);
	    	}
	    });	

	    RPG.socket.on('send_stop_monster_animation',function(data){
	    	var index = RPG.monsters_id.indexOf(data.id);
	    	if(index != -1)
	    	{
		    	RPG.monsters[index].sprite.animations.stop();
	    	}
	    });
	    var self = this;
	    RPG.socket.on('send_fire_monster',function(data){
	    	var index_monster = RPG.monsters_id.indexOf(data.id);
	    	var index_player = RPG.users_id.indexOf(data.id_player);
	    	if(index_monster != -1 && index_player != -1)
	    	{
		    	RPG.monsters[index_monster].fire(RPG.users[index_player],self.ennemy_bullets);
	    	}
	    });

	    RPG.socket.on('delete_monster',function(data){
	    	var index = RPG.monsters_id.indexOf(data.id);
	    	if(index != -1)
	    	{
	    		console.log('delete : '+data.id);
	    		RPG.monsters[index].sprite.kill();
	    		RPG.monsters_id.splice(index,1);
				RPG.monsters.splice(index,1);
	    	}
	    });

	    /************************************* Fin gestion monstres ***********************************************/

	    $(window).bind('beforeunload',function(){ // dès qu'un utilisateur ferme l'onglet, on informe le serveur de sa déconnexion
                RPG.socket.emit('disconnect',this.player.getId());
                this.player.savePlayer();
        });
	   
	    RPG.socket.on('delete_player',function(data){
	    	var index = RPG.users_id.indexOf(data.id);
	    	if(index != -1)
	    	{
	    		RPG.users[index].sprite.kill();
		    	RPG.users_id.splice(index, 1);
		    	RPG.users.splice(index,1);
		    	sessionStorage.removeItem('pseudo');
		    	sessionStorage.removeItem('username');
	    	}
	    });


	},

	bulletHitWall : function ( sprite1 , sprite2 )
	{
		sprite1.reset(-500 , -500);
	} ,

	bulletHitMonster : function (monster, bullets, type)
	{

		if (typeof(this.boss) != 'undefined')
		{
			if (monster == this.boss.sprite)
			{
				if (type == "bullets") {
					this.boss.damageReceived(this.player.damageDealt("Physical"));
					this.createDmgText(this.player.damageDealt("Physical"), monster, "Enemy");
				}
				else if (type == "laser") {
					this.boss.damageReceived(this.player.damageDealt("Magical"));
    				var explosion = this.add.sprite(monster.x-64, monster.y-64, 'explode');
    				explosion.animations.add('kaboom', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]);
    				explosion.play('kaboom', 16, false, true);
    				this.createDmgText(this.player.damageDealt("Magical"), monster, "Enemy");
				}
				else if (type == "fire") {
					this.boss.damageReceived(this.player.damageDealt("Magical"));
					var fire = this.add.sprite(bullets.x-32, bullets.y-32, 'fire');
					fire.animations.add('fire', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]);
					fire.play('fire', 9, false, true);
					this.createDmgText(this.player.damageDealt("Magical"), monster, "Enemy");
				}
				else if (type == "slash") {
					this.boss.damageReceived(this.player.damageDealt("Physical"));
					var slash = this.add.sprite(monster.x-32, monster.y-32, 'slash');
					slash.animations.add('slash', [14,15,16,17,18,19,20]);
					slash.play('slash', 7, false, true);
					this.createDmgText(this.player.damageDealt("Physical"), monster, "Enemy");
				}
				if (this.boss.hp <= 0) {
					this.player.incrementsExp(this.boss.exp);
				}
			}
		}
		else 
		{
			for (i = 0 ; i < RPG.monsters.length ; i++) {
				if (RPG.monsters[i].sprite == monster) {
					if (type == "bullets") {
						RPG.monsters[i].damageReceived(this.player.damageDealt("Physical"));
						this.createDmgText(this.player.damageDealt("Physical"), monster, "Enemy");
					}
					else if (type == "laser") {
						RPG.monsters[i].damageReceived(this.player.damageDealt("Magical"));
	    				var explosion = this.add.sprite(monster.x-64, monster.y-64, 'explode');
	    				explosion.animations.add('kaboom', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]);
	    				explosion.play('kaboom', 16, false, true);
	    				this.createDmgText(this.player.damageDealt("Magical"), monster, "Enemy");
					}
					else if (type == "fire") {
						RPG.monsters[i].damageReceived(this.player.damageDealt("Magical"));
						var fire = this.add.sprite(bullets.x-32, bullets.y-32, 'fire');
						fire.animations.add('fire', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]);
						fire.play('fire', 9, false, true);
						this.createDmgText(this.player.damageDealt("Magical"), monster, "Enemy");
					}
					else if (type == "slash") {
						RPG.monsters[i].damageReceived(this.player.damageDealt("Physical"));
						var slash = this.add.sprite(monster.x-32, monster.y-32, 'slash');
						slash.animations.add('slash', [14,15,16,17,18,19,20]);
						slash.play('slash', 7, false, true);
						this.createDmgText(this.player.damageDealt("Physical"), monster, "Enemy");
					}
					var j = i;
					i = this.monstern;
				}
			}
			var now = this.game.time.now;
			if (RPG.monsters[j].hp <= 0) {
				this.player.incrementsExp(RPG.monsters[j].exp);
				monster.reset(-500, -500);
			}
		}
		bullets.reset(-500, -500);
	} ,

	bulletHitPlayer : function ( player , ennemy_bullets )
	{
		var dmg = ennemy_bullets.damage;
		ennemy_bullets.reset(-500 , -500);
		var dmgReceived = this.player.damageReceived(dmg);
		this.createDmgText(dmgReceived, player, "Ally");
	} ,
	bossBulletHitPlayer : function ( player , ennemy_bullets  , n )
	{
		ennemy_bullets.reset(-500 , -500);
		if ( n ==  1)
		{
			var dmg = 20;
			var dmgReceived = this.player.damageReceived(dmg);
		}
		else if (n == 3)
		{
			var dmg = 7;
			var dmgReceived = this.player.damageReceived(dmg);
		}
		else if (n == 2)
		{
			var dmg = 15;
			var dmgReceived = this.player.damageReceived(dmg);
		}
		this.createDmgText(dmgReceived, player, "Ally");		
	} ,

	createDmgText: function(dmg, sprite, type) {
		var dmgText = this.game.add.text(sprite.x-8, sprite.y-32, dmg);
		dmgText.font = 'Roboto Slab';
		dmgText.fontSize = 20;
		dmgText.align = 'center';
		dmgText.stroke = '#000000';
		var grd = dmgText.context.createLinearGradient(0, 0, 0, dmgText.canvas.height);
		if (type == "Enemy") {
		    grd.addColorStop(0, '#8ED6FF');   
		    grd.addColorStop(1, '#004CB3');
		}
		else if (type == "Ally") {
			grd.addColorStop(0, '#EB9B9A');   
		    grd.addColorStop(1, '#B55A58');
		}
		dmgText.fill = grd;
		this.game.time.events.add(100, function() {
		    this.game.add.tween(dmgText).to({y: sprite.y-200}, 3000, Phaser.Easing.Linear.None, true);
		    this.game.add.tween(dmgText).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
		}, this);
	},

	checkOverlap :  function (spriteA, spriteB) {
	    var boundsA = spriteA.getBounds();
	    var boundsB = spriteB.getBounds();

	    return Phaser.Rectangle.intersects(boundsA, boundsB);

	} ,


	loadMap : function( tilemap,tileset, tileset_1 , image ,image_1, x , y)
	{
		if (typeof this.map != "undefined"){
			this.map.destroy()
			if (typeof this.uplayer != "undefined"){
				this.upperLayer.destroy()
				this.upLayer.destroy()}
			this.backgroundLayer.destroy()
			this.blockedLayer.destroy()			
		}
		//	Load current map

	    this.map = this.add.tilemap(tilemap);
	    this.map.addTilesetImage( tileset , image );
	    this.map.addTilesetImage( tileset_1 , image_1 );

	    // Place player with x y
	    this.player.sprite.position.x = x;
	    this.player.sprite.position.y = y;

	    //	Create group
	    this.upperLayer = this.add.group();
	    this.upperLayer.z = 0;
	    this.spriteLayer = this.add.group();
	    this.spriteLayer.z = 1;
	    this.monstreLayer = this.add.group();
	    this.monstreLayer.z = 1;

	    //	Create layer
	    this.upLayer = this.map.createLayer('UpperLayer');
	    this.backgroundLayer = this.map.createLayer('background');
	    this.blockedLayer = this.map.createLayer('block');

		//	Collision on block
	    this.blocked=this.blockedLayer.getTiles(0, 0, 1600, 1800, false, false)
	    var i,j=0;
	    while (i!=1){
	    	if (typeof this.blocked[j]!= "undefined"){
	    		if(this.blocked[j].index!=-1){
	    			this.map.setCollisionByIndex(this.blocked[j].index, true, this.blockedLayer.index)
	    		}
	    		j++
	    	}
	    	else{i=1;}
	    }
	    this.createMonsters(0);
	    //	Add layer to different group ; used for z position
	    this.upperLayer.add(this.backgroundLayer);
	    this.spriteLayer.add(this.player.sprite);
	    
	    for (i = 0 ; i < this.npcn ; i++)
	    {	    
	   		this.spriteLayer.add(this.npcT[i].sprite);
	   	}

	    for (i = 0 ; i < RPG.monsters.length ; i++)
	    {
	    	this.monstreLayer.add(RPG.monsters[i].sprite);
	    }
	    
	    this.backgroundLayer.resizeWorld();
	    this.player.addInterface();  

	    //Gère les projectiles du joueurs
	   	this.bullets = this.add.group();
	    this.bullets.enableBody = true;
	    this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
	    this.bullets.createMultiple(100, 'bullet2');
	    this.bullets.setAll('anchor.x', 0.5);
	    this.bullets.setAll('anchor.y', 0.5);
	    this.bullets.setAll('outOfBoundsKill', true);
	    this.bullets.setAll('checkWorldBounds', true);

	    
	    // gère les différents sorts du joueur
	    this.spell1 = this.add.group();
	    this.spell1.enableBody = true;
	    this.spell1.physicsBodyType = Phaser.Physics.ARCADE;
	    this.spell1.createMultiple(30, 'laser');
	    this.spell1.setAll('anchor.x', 0.5);
	    this.spell1.setAll('anchor.y', 0.5);
	    this.spell1.setAll('outOfBoundsKill', true);
	    this.spell1.setAll('checkWorldBounds', true);
	    
	    this.spell3 = this.add.group();
	    this.spell3.enableBody = true;
	    this.spell3.physicsBodyType = Phaser.Physics.ARCADE;
	    this.spell3.createMultiple(30, 'fire');
	    this.spell3.setAll('anchor.x', 0.5);
	    this.spell3.setAll('anchor.y', 0.5);
	    this.spell3.setAll('outOfBoundsKill', true);
	    this.spell3.setAll('checkWorldBounds', true);
	    
	    //Gère les projectiles des méchants
	   	this.ennemy_bullets = this.add.group();
	    this.ennemy_bullets.enableBody = true;
	    this.ennemy_bullets.physicsBodyType = Phaser.Physics.ARCADE;
	    this.ennemy_bullets.createMultiple(30, 'arrow');
	    this.ennemy_bullets.setAll('anchor.x', 0.5);
	    this.ennemy_bullets.setAll('anchor.y', 0.5);
	    this.ennemy_bullets.setAll('outOfBoundsKill', true);
	    this.ennemy_bullets.setAll('checkWorldBounds', true);

	    this.spriteLayer.add(this.ennemy_bullets);
	    this.spriteLayer.add(this.bullets);
	    this.spriteLayer.add(this.spell1);
	    this.spriteLayer.add(this.spell3);
	    this.getPlayersAndMonsters();

	    if (this.boss)
	    {
		    this.spriteLayer.add(this.boss.bullets1);
		    this.spriteLayer.add(this.boss.bullets2);
		    this.spriteLayer.add(this.boss.bullets3);
		    this.spriteLayer.add(this.boss.bullets4);
		    this.spriteLayer.add(this.boss.bullets5);
		}
	    for (i = 0 ; i < RPG.users.length ; i++)
	    {
	    	this.spriteLayer.add(RPG.users[i].sprite);
	    }
	} ,

	alsoExist : function(tab,id){
		for(var i =0;i<tab.length;i++)
		{
			if(tab[i] == id)
				return true;
		}
		return false;
	},

	getPlayersAndMonsters : function()
	{
		RPG.socket.emit('get_connected_players',RPG.current_map);
	    RPG.socket.emit('get_monsters_alive',RPG.current_map);
	    var self = this;
		RPG.socket.on('send_id_monster',function(data){
			if(RPG.move_in_monsters < RPG.monsters.length && self.alsoExist(RPG.monsters_id,data.id) == false)
			{
				RPG.monsters_id[RPG.move_in_monsters] = data.id;
		    	RPG.monsters[RPG.move_in_monsters].setId(data.id);
		    	RPG.move_in_monsters++;
			}
    	});
		var self = this;
		RPG.socket.on('return_connected_players',function(data){
			RPG.connected_players = JSON.parse(data); // on recupère les joueurs déjà sur la même map que l'utilisateur courant
			
			for(var i=0;i<RPG.connected_players.length;i++) // on fait apparaitre les joueurs étant déjà connectés et sur la même map que l'utilisateur courant
			{
				if(self.alsoExist(RPG.users_id,RPG.connected_players[i].id) == false) // on ajoute que les sprites dont l'id est différent de notre utilisateur courant
				{
					if(RPG.id != RPG.connected_players[i].id)
					{
						var player = RPG.connected_players[i];
						RPG.users_id.push(player.id);
						var expPercent = parseInt((player.exp/((player.level)*Math.pow(player.level,2))*100));
						RPG.users.push(new Character(self.game));
			    		var k = (RPG.users.length)-1;
			    		RPG.users[k].create(player.x,player.y,player.job,player.name ,player.level,player.exp,expPercent,player.currentHealth,player.strength,player.intelligence,player.stamina,player.dexterity,player.map,player.direction,player.orientation,false );
					}
				}	
			}
		}); 


		var self = this;
		RPG.socket.on('return_monsters_alive',function(data){
			RPG.monsters_alive = JSON.parse(data); 
			for(var i=0;i<RPG.monsters_alive.length;i++)
			{
				if(self.alsoExist(RPG.monsters_id,RPG.monsters_alive[i].id) == false)
				{
					RPG.monsters_id.push(RPG.monsters_alive[i].id);
		    		RPG.monsters.push(new Monster(self.game,RPG.monsters_alive[i].sprite_number,RPG.monsters_alive[i].x, RPG.monsters_alive[i].y));
		    		var k = (RPG.monsters.length)-1;
		    		RPG.monsters[k].create(RPG.monsters_alive[i].range ,RPG.monsters_alive[i].hp , RPG.monsters_alive[i].level, RPG.monsters_alive[i].exp ,RPG.current_map,RPG.monsters_alive[i].damage,false);
		    		RPG.monsters[k].setId(RPG.monsters_alive[i].id);
		    	}	
			}
		}); 
	},

	emptyAllTabs : function(){
		RPG.users = [];
		RPG.users_id = [];
		for(var i =0; i<RPG.monsters.length;i++)
		{	
			if(RPG.monsters[i].getMovable())
			{
				console.log('id to delete: '+RPG.monsters[i].getId());
				RPG.socket.emit('kill_monster',RPG.monsters[i].getId());
			}
		}
		RPG.monsters = [];
		RPG.monsters_id = [];
	},

	checkMapTransition : function ()
	{
			//accès mairie
		if (RPG.current_map=='level6') {
			if(this.player.sprite.x>750 ) {
				this.emptyAllTabs();
				RPG.current_map='level5';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);	
				this.loadMap( 'level5' , 'batiment1','rue' , 'batiment','rue' , 51 , 357 );			
			}
			else if(this.player.sprite.y<480 && this.player.sprite.x>320 && this.player.sprite.x<380){
				this.emptyAllTabs();
				RPG.current_map='level7';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);	
				this.loadMap( 'level7' , 'donjon_1','donjon_1' , 'donjon_s1','donjon_s1' , 360 , 1750 );
			}
		}
		//depuis la base aero
		else if (RPG.current_map=='level3') {
			if(this.player.sprite.y<50){
				this.emptyAllTabs();
				this.map.destroy();
				RPG.current_map='level5';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);	
				this.loadMap( 'level5' , 'batiment1','rue' , 'batiment','rue' , 241 , 1171 );
			}
			//accés foret 
			else if(this.player.sprite.y<364 && this.player.sprite.y>214 && this.player.sprite.x<50){		
				this.emptyAllTabs();
				RPG.current_map='level4';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);	
				this.loadMap( 'level4' , 'batiment1','rue' , 'batiment','rue' , 1548 , 230 );
				
			}
			else if(this.player.sprite.y<1500 && this.player.sprite.y>1270 && this.player.sprite.x<50){		
				this.emptyAllTabs();
				RPG.current_map='level2';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);	
				this.loadMap( 'level2' , 'batiment1','rue' , 'batiment','rue' , 750 ,450);
				
			}
		}//deouis la mairie
		else if (RPG.current_map=='level5') {
			//accés base aerospatiale
			if(this.player.sprite.y>1200){
				this.emptyAllTabs();
				RPG.current_map='level3';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);
				this.loadMap( 'level3' , 'batiment1','rue' , 'batiment','rue' , 248 , 119 );					
			}
			//accés carriere
			else if(this.player.sprite.y<463 && this.player.sprite.y>244 && this.player.sprite.x<50){		
				this.emptyAllTabs();
				RPG.current_map='level6';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);	
				this.loadMap( 'level6' , 'batiment1','rue' , 'batiment','rue' , 750 , 300 );
			}
		}//depuis 
		else if (RPG.current_map=='level4') {
			if(this.player.sprite.y<288 && this.player.sprite.y>172 && this.player.sprite.x>1550){		
				this.emptyAllTabs();
				RPG.current_map='level3';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);	
				this.loadMap( 'level3' , 'batiment1','rue' , 'batiment','rue' , 105 , 274 );
			}
		}
		else if (RPG.current_map=='level4') {
			if(this.player.sprite.y<288 && this.player.sprite.y>172 && this.player.sprite.x>1550){		
				this.emptyAllTabs();
				RPG.current_map='level3';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);	
				this.loadMap( 'level3' , 'batiment1','rue' , 'batiment','rue' , 105 , 274 );
			}
		}	
		else if(RPG.current_map=='level1'){
			if(this.player.sprite.y>560){
				this.emptyAllTabs();		
				RPG.current_map='level2';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);	
				this.loadMap( 'level2' , 'batiment1','rue' , 'batiment','rue' , 325 , 300 );
				if(this.player.stage==0){
				blabla= ["J'ai besoin de vous pour désinfecter la zone nord*dans l'usine mais entraînez vous dans la forêt avant !" ];
				this.createnpc(blabla,"1","Mme la Maire",375,300);
			}
			}
		}
		else if(RPG.current_map=='level2'){
			if(this.player.sprite.x>750){
				this.emptyAllTabs();
				RPG.current_map='level3';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);	
				this.loadMap( 'level3' , 'batiment1','rue' , 'batiment','rue' , 55 , 1320 );
			}
		}
		else if(RPG.current_map=='level7'){
			if(this.player.sprite.y<60){
				this.emptyAllTabs();
				RPG.current_map='level8';
				this.player.setMap(RPG.current_map);
				RPG.socket.emit('change_map',{id : this.player.getId(), map : RPG.current_map});
				this.createMonsters(1);
				this.loadMap( 'level8' , 'donjon_2','donjon_2' , 'donjon_s2','donjon_s2' , 380 , 550 );
			}
		}	 
	},

	createnpc: function(bla,type,name,x,y) {
		this.npcn=0
		this.npcT[0]= new Npc (this.game);
		this.npcT[0].preload(type);
		this.npcT[0].create(x , y , type , bla , name);
			
	},
	createMonsters: function(n) {
		if (RPG.current_map != 'level1') {
			this.monstern = n;
			for (i=0 ; i<this.monstern ; i++) {
			   	RPG.monsters.push(new Monster(this.game, 2, Math.random()*800, Math.random()*600));
			   	var k = (RPG.monsters.length)-1;
		   		RPG.monsters[k].create(75, 25, 1, 2,RPG.current_map, 7,true);
			}
		}
	},
	createTextbox : function ()
	{
		
		this.textbox = this.game.add.sprite(this.game.camera.x + 15 , this.game.camera.y+325 ,'textbox');
		this.textbox.z = 0;
		this.username = this.game.add.text(this.game.camera.x +60, this.game.camera.y+350 , '' , { font: "30px Arial", fill: "#ffffff", align: "center" });
		this.username.setText("Narrateur");
		this.textL1 = this.game.add.text(this.game.camera.x +40, this.game.camera.y+425, '' , { font: "30px Arial", fill: "#ffffff", align: "center" });
		this.textL2 = this.game.add.text(this.game.camera.x +40, this.game.camera.y+475, '' , { font: "30px Arial", fill: "#ffffff", align: "center" });
		this.textL3 = this.game.add.text(this.game.camera.x +40, this.game.camera.y+525, '' , { font: "30px Arial", fill: "#ffffff", align: "center" });
    	this.isSpeaking = true;
    	return true;
	} ,
	writeLetter : function( )
	{
		this.delay(15);
		if (this.index >= this.content[this.currentScene].length)
		{
			this.index = 0;
			this.lineNumber = 1;
			this.isSpeaking = false;
			this.dialogWait = true;
		}
		else if (this.lineNumber == 1 && this.isSpeaking == true)
		{
			this.actualText += this.content[this.currentScene][this.index];
			this.textL1.setText(this.actualText);
			this.index++;
			if (this.content[this.currentScene][this.index] == '*')
			{
				this.index++;
				this.lineNumber = 2;
				this.actualText = "";
			}
		}
		else if (this.lineNumber == 2)
		{
			this.actualText += this.content[this.currentScene][this.index];
			this.textL2.setText(this.actualText);
			this.index++;
			if (this.content[this.currentScene][this.index] == '*')
			{
				this.index++;
				this.lineNumber = 3;
				this.actualText = "";
			}
		}
		else if (this.lineNumber == 3)
		{
			this.actualText += this.content[this.currentScene][this.index];
			this.textL3.setText(this.actualText);
			this.index++;
			if (this.content[this.currentScene][this.index] == '*')
			{
				this.dialogWait = true;
			}
		}
	} , 

	newPage : function()
	{
		this.index++;
		this.actualText = "";
		this.lineNumber = 1;
		this.dialogWait = false;
		this.textL1.setText("");
		this.textL2.setText("");
		this.textL3.setText("");
	} ,

	endDialog : function()
	{
		this.index = 0;
		this.textbox.kill();
		this.actualText = "";
		this.lineNumber = 1;
		this.dialogWait = false;
		this.username.setText("");
		this.textL1.setText("");
		this.textL2.setText("");
		this.textL3.setText("");

	} ,
	//En milliseconde
	delay : function ( time )
	{
		var date = new Date();
		var curDate = null;
		do { curDate = new Date(); }
		while(curDate-date < time );
	}  
};