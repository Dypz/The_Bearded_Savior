var Monster = function(game,number,x,y) {
  console.log('new monster : '+number);
  this.game = game;
  this.id = null;
  this.sprite_number = number;
  this.sprite = this.game.add.sprite(x,y,'monster'+number);
  this.alive = true;
  this.tilex = 0;
  this.tiley = 0;
  this.damage;
  this.orientation = 1;
  this.map;
  this.movable;
};

Monster.prototype = {
  //X position ; y position ; sa portée de tir (mettre 0 pour corps à corps ?) ; et son Id
  //La range est évalué au carré par exemple 42 donne environ 32 pixel d'écarts
  create: function(range, hp, level, exp, map,damage,send_to_others) {
    //Création de notre personnage
	this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
  this.sprite.anchor.setTo(0.5,0.5);

  this.sprite.animations.add('down', [0, 1, 2, 3], 6, true , true);	//2
	this.sprite.animations.add('left', [4, 5, 6, 7], 6, true , true);	//3
  this.sprite.animations.add('right', [8, 9, 10, 11], 6, true , true);	//4
	this.sprite.animations.add('up', [12, 13, 14, ], 6, true , true);	//1
	this.sprite.direction = 'down';

	//Collision sur les bords
	this.sprite.body.collideWorldBounds = true;
	this.range = range;
  this.fireRate = 1000;
  this.nextFire = 0;
  this.hp = hp;
  this.level = level;
  this.exp = exp;
  this.damage = damage;
  this.map = map;
  this.setMovable(send_to_others);
  console.log('create monster '+this.map);

  if(send_to_others == true)
  {
    RPG.socket.emit('new_monster',{x : this.sprite.x , y : this.sprite.y , range : this.range, hp : this.hp, level : this.level, exp : this.exp, orientation : this.orientation , direction : this.sprite.direction , map : this.map, damage : this.damage, sprite_number : this.sprite_number  });
  }	
},

  update: function(player , bullets) {
  	if (player.isTalking == false) {
  			if(this.movable == true)
    			this.movements(player , bullets);
    }
  },

  reposition: function() {
    this.sprite.x = this.tilex;
    this.sprite.y = this.tiley;
  },

  setId : function(id){
  	this.id = id;
  },

  getId : function(){
  	return this.id;
  },

  getMovable : function(){
    return this.movable;
  },

  setMovable : function(movable){
    this.movable = movable;
  },

  movements:  function( player , bullets) {
    this.sprite.body.velocity.x = 0;
    this.sprite.body.velocity.y = 0;
    var speed = 50;
    var marge = 5;
    //Le monstre vous poursuis jusqu'à ce qu'il puisse faire feu sur vous. Parce que le monstre vous déteste
    if (this.isAlive()) //S'il est vivant, c'est qu'il est mort !
    {
    	if ( this.checkRange(player) == true)
    	{	
	    	this.sprite.animations.stop();
    		this.fire(player , bullets);
  			RPG.socket.emit('fire_monster',{ id : this.getId(),id_player : player.getId()} );
  			RPG.socket.emit('stop_monster_animation',this.getId());
    	}
    	else
    	{
		    if ( (player.sprite.x+marge > this.sprite.x) && ( player.sprite.x-marge < this.sprite.x ) && (player.sprite.y > this.sprite.y) ) 
	    	{
		    	this.sprite.body.velocity.y = -speed;
	    		this.sprite.animations.play('up');
    			RPG.socket.emit('animation_monster_server',{id: this.getId(),direction: 'up'});
    			RPG.socket.emit('actualise_position_monster_server',{id: this.getId(), x : this.sprite.x,y : this.sprite.y});
	    	}
	    	else if ( (player.sprite.x+marge > this.sprite.x) && ( player.sprite.x-marge < this.sprite.x ) && (player.sprite.y < this.sprite.y) ) 
	    	{
		    	this.sprite.body.velocity.y = speed;
	    		this.sprite.animations.play('down');
    			RPG.socket.emit('animation_monster_server',{id: this.getId(),direction: 'down'});
    			RPG.socket.emit('actualise_position_monster_server',{id: this.getId(), x : this.sprite.x,y : this.sprite.y});
	    	}
	    	else if (player.sprite.x < this.sprite.x)
	    	{
		    	this.sprite.body.velocity.x = -speed;
		    	this.sprite.animations.play('left');
    			RPG.socket.emit('animation_monster_server',{id: this.getId(),direction: 'left'});
    			RPG.socket.emit('actualise_position_monster_server',{id: this.getId(), x : this.sprite.x,y : this.sprite.y});
		    }
		    else if (player.sprite.x > this.sprite.x)
		    {
		    	this.sprite.body.velocity.x = speed;
		    	this.sprite.animations.play('right');
    			RPG.socket.emit('animation_monster_server',{id: this.getId(),direction: 'right'});
    			RPG.socket.emit('actualise_position_monster_server',{id: this.getId(), x : this.sprite.x,y : this.sprite.y});
		    }

		    if ( (player.sprite.y+marge > this.sprite.y) && ( player.sprite.y-marge < this.sprite.y ) && (player.sprite.x < this.sprite.x) ) 
		    {
		    	this.sprite.body.velocity.x = -speed;
		    	this.sprite.animations.play('left');
    			RPG.socket.emit('animation_monster_server',{id: this.getId(),direction: 'left'});
    			RPG.socket.emit('actualise_position_monster_server',{id: this.getId(), x : this.sprite.x,y : this.sprite.y});
		    }
		    else if ( (player.sprite.y+marge > this.sprite.y) && ( player.sprite.y-marge < this.sprite.y ) && (player.sprite.x > this.sprite.x) ) 
		    {
		    	this.sprite.body.velocity.x = speed;
		    	this.sprite.animations.play('right');
	    		RPG.socket.emit('animation_monster_server',{id: this.getId(),direction: 'right'});
	    		RPG.socket.emit('actualise_position_monster_server',{id: this.getId(), x : this.sprite.x,y : this.sprite.y});
		    }    
		    else if (player.sprite.y < this.sprite.y)
		    {
		    	this.sprite.body.velocity.y = -speed;
		    	this.sprite.animations.play('up');
    			RPG.socket.emit('animation_monster_server',{id: this.getId(),direction: 'up'});
    			RPG.socket.emit('actualise_position_monster_server',{id: this.getId(), x : this.sprite.x,y : this.sprite.y});
		    }
		    else if (player.sprite.y > this.sprite.y)
		    {
		    	this.sprite.body.velocity.y = speed;
		    	this.sprite.animations.play('down');
    			RPG.socket.emit('animation_monster_server',{id: this.getId(),direction: 'down'});
    			RPG.socket.emit('actualise_position_monster_server',{id: this.getId(), x : this.sprite.x,y : this.sprite.y});
		    }
		}
	}
  } , 
  
	checkRange: function (player)	//Retourne 1 si le monstre est à portée ; 0 sinon
	{
		if ((player.sprite.x - this.sprite.x)*(player.sprite.x - this.sprite.x) + (player.sprite.y - this.sprite.y)*(player.sprite.y - this.sprite.y) < (this.range*this.range))
		{
			return true;
		}
		else 
		{
			return false;
		}
	} , 
	fire: function ( player , bullets )
	{
 		if (this.game.time.now > this.nextFire && bullets.countDead() > 0)
        {
            this.nextFire = this.game.time.now + this.fireRate;

            var bullet = bullets.getFirstExists(false);

            bullet.lifespan = 2000;

            bullet.reset(this.sprite.x, this.sprite.y);

            bullet.rotation = this.game.physics.arcade.moveToObject(bullet, player.sprite, 300);
            bullet.damage = this.damage;
        }
	} , 
	isAlive : function ()
	{
		//Notre monstre est-il en vie ?
		if (this.hp <= 0)
		{
			this.sprite.kill();
			var index = RPG.monsters_id.indexOf(this.getId());
	    	if(index != -1)
	    	{
	    		console.log('delete : '+this.getId());
	    		RPG.monsters_id.splice(index,1);
  				RPG.monsters.splice(index,1);
  				RPG.move_in_monsters--;	
  				RPG.socket.emit('kill_monster',this.getId());
	    	}
		}
		else
		{
			return true;
		}
	},
	damageReceived: function(dmg) {
		this.hp -= dmg;
	}
};