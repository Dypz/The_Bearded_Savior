var Player = function(start_x,start_y,start_job,start_name,start_level,start_exp,start_orientation,start_direction,start_frame,start_strength,start_dexterity,start_intelligence,start_maxHealth,start_currentHealth,start_stamina,start_map){
	var x = start_x,
		y = start_y,
		job = start_job,
		name = start_name,
		level = start_level,
		exp = start_exp,
		currentHealth = start_currentHealth,
		orientation = start_orientation,
		direction = start_direction,
		frame = start_frame,
		strength = start_strength,
		dexterity = start_dexterity,
		maxHealth = start_maxHealth,
		currentHealth = start_currentHealth,
		intelligence = start_intelligence,
		stamina = start_stamina,
		map = start_map,
		id;

	var getX = function(){
		return x;
	};

	var getY = function(){
		return y;
	};

	var setX = function(new_x){
		x = new_x;
	};

	var setY = function(new_y){
		y = new_y;
	};

	var getJob = function(){
		return job;
	};

	var setJob = function(new_job){
		job = new_job;
	};

	var getName = function(){
		return name;
	};

	var setName = function(new_name){
		name = new_name;
	};

	var getLevel = function(){
		return level;
	};

	var setLevel = function(new_level){
		level = new_level;
	};

	var getExp = function(){
		return exp;
	};

	var setExp = function(new_exp){
		exp = new_exp;
	};

	var getOrientation = function(){
		return orientation;
	};

	var setOrientation = function(new_orientation){
		orientation = new_orientation;
	};

	var getDirection = function(){
		return direction;
	};

	var setDirection = function(new_direction){
		direction = new_direction;
	};

	var getFrame = function(){
		return frame;
	};

	var setFrame = function(new_frame){
		frame = new_frame;
	};

	var getStrength = function(){
		return strength;
	};

	var setStrength = function(new_strength){
		strength = new_strength;
	};

	var getDexterity = function(){
		return dexterity;
	};

	var setDexterity = function(new_dexterity){
		dexterity = new_dexterity;
	};

	var getIntelligence = function(){
		return intelligence;
	};

	var setIntelligence = function(new_intelligence){
		intelligence= new_intelligence;
	};

	var getStamina = function(){
		return stamina;
	};

	var setStamina = function(new_stamina){
		stamina = new_stamina;
	};

	var getMaxHealth = function(){
		return maxHealth;
	};

	var setMaxHealth = function(new_maxHealth){
		maxHealth = new_maxHealth;
	};

	var getCurrentHealth = function(){
		return currentHealth;
	};

	var setCurrentHealth = function(new_currentHealth){
		currentHealth = new_currentHealth;
	};

	var getMap = function(){
		return map;
	};

	var setMap = function(new_map){
		map = new_map;
	};

	return {
		getX: getX,
		getY: getY,
		setX: setX,
		setY: setY,
		getJob: getJob,
		setJob: setJob,
		getName: getName,
		setName : setName,
		getLevel: getLevel,
		setLevel: setLevel,
		getExp: getExp,
		setExp: setExp,
		getOrientation: getOrientation,
		setOrientation: setOrientation,
		getDirection: getDirection,
		setDirection: setDirection,
		getFrame: getFrame,
		setFrame: setFrame,
		getStrength: getStrength,
		setStrength: setStrength,
		getDexterity: getDexterity,
		setDexterity: setDexterity,
		getIntelligence: getIntelligence,
		setIntelligence: setIntelligence,
		getStamina: getStamina,
		setStamina: setStamina,
		getMaxHealth: getMaxHealth,
		setMaxHealth: setMaxHealth,
		getCurrentHealth: getCurrentHealth,
		setCurrentHealth: setCurrentHealth,
		setMap: setMap,
		getMap: getMap,
		id: id
	}
};
exports.Player = Player;