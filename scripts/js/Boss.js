/*global Game*/
/*global tileSize*/
/*global dialogue*/


var Boss = function(game) {
  this.game = game;
  this.sprite = null;
  this.alive = true;
  this.tilex = 0;
  this.tiley = 0;
  this.orientation = 1;
  this.contact = false;
};

Boss.prototype = {
  //X position ; y position ; sa portée de tir (mettre 0 pour corps à corps ?) ; et son Id
  //La range est évalué au carré par exemple 42 donne environ 32 pixel d'écartzaA
  create: function( x , y , Range , Id , hp, lvl, exp , player) {
    //Création de notre personnage
    this.createProjectile();
    
    this.sprite = this.game.add.sprite(x,y,'boss');
	this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
    this.sprite.anchor.setTo(0.5,0.5);

    this.sprite.animations.add('down', [0, 1, 2, 3], 6, true , true);	//2
	this.sprite.animations.add('left', [4, 5, 6, 7], 6, true , true);	//3
    this.sprite.animations.add('right', [8, 9, 10, 11], 6, true , true);	//4
	this.sprite.animations.add('up', [12, 13, 14, ], 6, true , true);	//1

	//Collision sur les bords
	this.sprite.body.collideWorldBounds = true;
	this.range = Range;

    this.fireRate = 1500;
    this.nextFire = 0;
    this.hp = hp;
    this.level = lvl;
    this.exp = exp;

    //Boss équipement
    this.fireMissileCooldown = 10000;
    this.fireSprayCooldown = 4000;
    this.fireRateSpray = 333;
    this.fireRateMissile = 400;
	this.activationMissile = false;
	this.activationSpray = false;
	this.activationTimeMissile = 2000;
	this.activationTimeSpray = 999;

	this.player = player;


  },

  update: function(player) {
  	if (player.isTalking == false) {
    	this.movements(player);
    }
    if ( this.activationMissile == false )
    {
    	this.activationMissile = true;
    	this.loopMissile1 = this.game.time.events.loop(this.fireMissileCooldown , function () 
    	{
			this.loopMissile2 = this.game.time.events.loop(this.fireRateMissile , function ()
			{
				this.missileLauncher(player);
			} , this);
			this.game.time.events.add(this.activationTimeMissile , function () {this.game.time.events.remove(this.loopMissile2);} , this);
    	} , this);
    } 
 	if ( this.activationSpray == false )
    {
    	this.activationSpray = true;
    	this.loopSpray1 = this.game.time.events.loop(this.fireSprayCooldown , function () 
    	{
			this.loopSpray2 = this.game.time.events.loop(this.fireRateSpray , function ()
			{
				this.fireSpray();
			} , this);
			this.game.time.events.add(this.activationTimeSpray , function () {this.game.time.events.remove(this.loopSpray2);} , this);
    	} , this);
    } 

  },

  reposition: function() {
    this.sprite.x = this.tilex;
    this.sprite.y = this.tiley;
  },

  movements:  function( player) {
    this.sprite.body.velocity.x = 0;
    this.sprite.body.velocity.y = 0;
    var speed = 25;
    var marge = 40;
    //Le monstre vous poursuis jusqu'à ce qu'il puisse faire feu sur vous. Parce que le monstre vous déteste
    if (this.isAlive()) //S'il est vivant, c'est qu'il n'est pas mort !
    {
    	if ( this.game.physics.arcade.distanceBetween(this.sprite , player.sprite) < this.range)
    	{	
    		this.fire( player );
    	}
	    if ( (player.sprite.x+marge > this.sprite.x) && ( player.sprite.x-marge < this.sprite.x ) && (player.sprite.y > this.sprite.y) ) 
    	{
	    	this.sprite.body.velocity.y = speed;
    		this.sprite.animations.play('down');
    	}
    	else if ( (player.sprite.x+marge > this.sprite.x) && ( player.sprite.x-marge < this.sprite.x ) && (player.sprite.y < this.sprite.y) ) 
    	{
	    	this.sprite.body.velocity.y = -speed;
    		this.sprite.animations.play('up');
    	}
    	else if (player.sprite.x < this.sprite.x)
    	{
	    	this.sprite.body.velocity.x = -speed;
	    	this.sprite.animations.play('left');
	    }
	    else if (player.sprite.x > this.sprite.x)
	    {
	    	this.sprite.body.velocity.x = speed;
	    	this.sprite.animations.play('right');
	    }
	}
  } , 
  
	checkRange: function (player)	//Retourne 1 si le monstre est à portée ; 0 sinon
	{
		if ((player.sprite.x - this.sprite.x)*(player.sprite.x - this.sprite.x) + (player.sprite.y - this.sprite.y)*(player.sprite.y - this.sprite.y) < (this.range*this.range))
		{
			return true;
		}
		else 
		{
			return false;
		}
	} , 
	fire: function ( player )
	{
 		if (this.game.time.now > this.nextFire && this.bullets1.countDead() > 0)
        {
            this.nextFire = this.game.time.now + this.fireRate;

            var bullet = this.bullets1.getFirstExists(false);

            bullet.lifespan = 2000;

            bullet.reset(this.sprite.x, this.sprite.y);

            bullet.rotation = this.game.physics.arcade.moveToObject(bullet, player.sprite, 250);
        }
	} , 
	isAlive : function ()
	{
		//Notre monstre est-il en vie ?
		if (this.hp <= 0)
		{
			this.sprite.kill();
			this.defeated();
		}
		else
		{
			return true;
		}
	},
	damageReceived: function(dmg) {
		this.hp -= dmg;
	} , 
	createProjectile : function ()
	{
		 //Gère les projectiles des méchants
	   	this.bullets1 = this.game.add.group();
	    this.bullets1.enableBody = true;
	    this.bullets1.physicsBodyType = Phaser.Physics.ARCADE;
	    this.bullets1.createMultiple(30, 'bullet3');
	    this.bullets1.setAll('anchor.x', 0.5);
	    this.bullets1.setAll('anchor.y', 0.5);
	    this.bullets1.setAll('outOfBoundsKill', true);
	    this.bullets1.setAll('checkWorldBounds', true);

	    //Gère les projectiles des méchants
	   	this.bullets2 = this.game.add.group();
	    this.bullets2.enableBody = true;
	    this.bullets2.physicsBodyType = Phaser.Physics.ARCADE;
	    this.bullets2.createMultiple(30, 'bullet1');
	    this.bullets2.setAll('anchor.x', 0.5);
	    this.bullets2.setAll('anchor.y', 0.5);
	    this.bullets2.setAll('outOfBoundsKill', true);
	    this.bullets2.setAll('checkWorldBounds', true);

	    //Gère les projectiles des méchants
	   	this.bullets3 = this.game.add.group();
	    this.bullets3.enableBody = true;
	    this.bullets3.physicsBodyType = Phaser.Physics.ARCADE;
	    this.bullets3.createMultiple(60, 'bullet4');
	    this.bullets3.setAll('anchor.x', 0.5);
	    this.bullets3.setAll('anchor.y', 0.5);
	    this.bullets3.setAll('outOfBoundsKill', true);
	    this.bullets3.setAll('checkWorldBounds', true);

	    //Gère les projectiles des méchants
	   	this.bullets4 = this.game.add.group();
	    this.bullets4.enableBody = true;
	    this.bullets4.physicsBodyType = Phaser.Physics.ARCADE;
	    this.bullets4.createMultiple(30, 'bullet5');
	    this.bullets4.setAll('anchor.x', 0.5);
	    this.bullets4.setAll('anchor.y', 0.5);
	    this.bullets4.setAll('outOfBoundsKill', true);
	    this.bullets4.setAll('checkWorldBounds', true);

	    //Gère les projectiles des méchants
	   	this.bullets5 = this.game.add.group();
	    this.bullets5.enableBody = true;
	    this.bullets5.physicsBodyType = Phaser.Physics.ARCADE;
	    this.bullets5.createMultiple(50, 'bullet6');
	    this.bullets5.setAll('anchor.x', 0.5);
	    this.bullets5.setAll('anchor.y', 0.5);
	    this.bullets5.setAll('outOfBoundsKill', true);
	    this.bullets5.setAll('checkWorldBounds', true);
	} , 
	closeContact : function ( player )
	{
		if (this.contact == true)
		{
		
		}
		else
		{
		this.contact = true;
		var marge = 25;
		var speed = 2000;
	    if ( (player.sprite.x+marge > this.sprite.x) && ( player.sprite.x-marge < this.sprite.x ) && (player.sprite.y > this.sprite.y) ) 
    	{
    		this.myloop =  this.game.time.events.loop(50, function () {player.sprite.body.velocity.y = speed;player.sprite.body.velocity.x = this.game.rnd.between(-700, 700);} , this);
	    	
    		//this.sprite.animations.play('down');
    	}
    	else if ( (player.sprite.x+marge > this.sprite.x) && ( player.sprite.x-marge < this.sprite.x ) && (player.sprite.y < this.sprite.y) ) 
    	{
	    	this.myloop = this.game.time.events.loop(50, function () {player.sprite.body.velocity.y = -speed;player.sprite.body.velocity.x = this.game.rnd.between(-700, 700);} , this);
    		//this.sprite.animations.play('up');
    	}
    	else if (player.sprite.x < this.sprite.x)
    	{
	    	this.myloop = this.game.time.events.loop(50, function () {player.sprite.body.velocity.x = -speed;player.sprite.body.velocity.y = this.game.rnd.between(-700, 700);} , this);
	    	//this.sprite.animations.play('left');
	    }
	    else if (player.sprite.x > this.sprite.x)
	    {
	    	this.myloop = this.game.time.events.loop(50, function () {player.sprite.body.velocity.x = speed;player.sprite.body.velocity.y = this.game.rnd.between(-700, 700);} , this);
	    	//this.sprite.animations.play('right');
	    }
	    this.game.time.events.add(100 , function () {this.game.time.events.remove(this.myloop); player.damageReceived(10); this.contact = false;} , this);
		}
	} , 
	missileLauncher : function ( player )
	{
            var bullet1 = this.bullets3.getFirstExists(false);
            this.game.physics.arcade.enable(bullet1);
            bullet1.lifespan = 2000;
            bullet1.reset(this.sprite.x + this.game.rnd.between(-20, 20), this.sprite.y + this.game.rnd.between(-30, 30));
			bullet1.rotation = this.game.physics.arcade.moveToObject(bullet1, player.sprite, 150 + this.game.rnd.between(0 , 300));
	} , 
	fireSpray : function()
	{
            this.nextFireSpray = this.game.time.now + this.fireRateSpray;

            var bullet1 = this.bullets5.getFirstExists(false);
            this.game.physics.arcade.enable(bullet1);
            bullet1.lifespan = 2000;
            bullet1.reset(this.sprite.x, this.sprite.y);
            bullet1.rotation = -(Math.PI)/4;
            bullet1.body.velocity.x = 300;
            bullet1.body.velocity.y = -300;

            var bullet2 = this.bullets5.getFirstExists(false);
            this.game.physics.arcade.enable(bullet2);
            bullet2.lifespan = 2000;
            bullet2.reset(this.sprite.x, this.sprite.y);
            bullet2.rotation = 0;
       		bullet2.body.velocity.x = 300;

            var bullet3 = this.bullets5.getFirstExists(false);          
           	this.game.physics.arcade.enable(bullet3); 
            bullet3.lifespan = 2000;        
            bullet3.reset(this.sprite.x, this.sprite.y);
			bullet3.rotation = -(Math.PI)/2; 
       		bullet3.body.velocity.y = -300;
       		
            var bullet4 = this.bullets5.getFirstExists(false);
            this.game.physics.arcade.enable(bullet4);
            bullet4.lifespan = 2000;
            bullet4.reset(this.sprite.x, this.sprite.y);
            bullet4.rotation = -(3/4)*(Math.PI);
            bullet4.body.velocity.x = -300;
            bullet4.body.velocity.y = -300;
            
            var bullet5 = this.bullets5.getFirstExists(false);
            this.game.physics.arcade.enable(bullet5);
            bullet5.lifespan = 2000;
            bullet5.reset(this.sprite.x, this.sprite.y);
            bullet5.rotation = (Math.PI);
       		bullet5.body.velocity.x = -300;

       		
            var bullet6 = this.bullets5.getFirstExists(false);          
           	this.game.physics.arcade.enable(bullet6); 
            bullet6.lifespan = 2000;        
            bullet6.reset(this.sprite.x, this.sprite.y);
			bullet6.rotation = (3/4)*(Math.PI); 
       		bullet6.body.velocity.y = 300;
       		bullet6.body.velocity.x = -300;
       		
            var bullet7 = this.bullets5.getFirstExists(false);
            this.game.physics.arcade.enable(bullet7);
            bullet7.lifespan = 2000;
            bullet7.reset(this.sprite.x, this.sprite.y);
            bullet7.rotation = (Math.PI)/2;
            bullet7.body.velocity.y = 300;
			
            var bullet8 = this.bullets5.getFirstExists(false);
            this.game.physics.arcade.enable(bullet8);
            bullet8.lifespan = 2000;
            bullet8.reset(this.sprite.x, this.sprite.y);
            bullet8.rotation = (Math.PI)/4;
       		bullet8.body.velocity.x = 300;
       		bullet8.body.velocity.y = 300;
       		
            //bullet.rotation = this.game.physics.arcade.moveToObject(bullet, player.sprite, 250);
	} , 
	defeated : function()
	{
		this.sprite.reset(-5000 , - 5000);
		this.game.time.events.add(10 , function () {this.game.time.events.remove(this.loopMissile1);} , this);
		this.game.time.events.add(10 , function () {this.game.time.events.remove(this.loopSpray1);} , this);
	} , 
	explode : function ( sprite1 )
	{

		this.boom =  this.game.add.sprite('fire');
	    sprite1.addChild(this.boom);
		sprite1.animations.add('fire', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]);
    	sprite1.play('fire', 6, false, true);
	}
};