RPG.Preloader = function(game) {
	RPG.gameLength = 800;
	RPG.gameWidth = 600;
};

RPG.Preloader.prototype = {
	preload: function() {
		this.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
		this.game.load.image('textbox','assets/images/textboxF.png',64,64);
		this.load.image('MenuBackground', './assets/images/MenuBackground.png');
		this.load.image('CSBackground', './assets/images/CSBackground.png');
		this.load.image('CCBackground', './assets/images/CCBackground.png')
		this.load.image('bouton1up','./assets/buttons/boutonup.png');
		this.load.image('bouton2up','./assets/buttons/bouton2up.png');
		this.load.image('bouton1down','./assets/buttons/boutondown.png');
		this.load.image('bouton2down','./assets/buttons/bouton2down.png');
		this.load.image('bouton_create','./assets/buttons/create.png');
		this.load.image('bouton_delete','./assets/buttons/delete.png');
		this.load.image('fleche','./assets/buttons/fleche.png');
		this.load.image('fleche_L','./assets/buttons/fleche_L.png');
		this.load.image('profil','./assets/buttons/profil.png');
		this.load.image('profile','./assets/buttons/stats.png');
		this.load.image('skill','./assets/buttons/skill.png');
		this.load.image('save','./assets/buttons/save.png');
		this.load.image('quit','./assets/buttons/quit.png');
		this.load.image('exit','./assets/buttons/exit.png');
		this.load.image('start_cc','./assets/buttons/start2.png');
		this.load.image('berseker','./assets/buttons/berseker.png');
		this.load.image('neogician','./assets/buttons/neogician.png');
		this.load.image('banderole','./assets/images/banderole.png');
		this.load.image('bouton_start','./assets/buttons/start.png');
		this.load.image('laser', './assets/sprites/projectile/laser.png');
		this.load.image('arrow', './assets/sprites/projectile/arrow.png');
		this.load.image('bullet1', './assets/sprites/projectile/bullet2.png');
		this.load.image('bullet2', './assets/sprites/projectile/bullet2.png');
		this.load.image('sky' , './assets/images/sky.png');
		this.load.image('bullet1', './assets/sprites/projectile/bullet1.png');
		this.load.image('bullet2', './assets/sprites/projectile/bullet2.png');
		this.load.image('bullet3', './assets/sprites/projectile/bullet3.png');
		this.load.image('bullet4', './assets/sprites/projectile/bullet4.png');
		this.load.image('bullet5', './assets/sprites/projectile/bullet5.png');
		this.load.image('bullet6', './assets/sprites/projectile/bullet6.png');
		this.load.image('fire1' , './assets/images/fire1.png');
		this.load.image('fire2' , './assets/images/fire2.png');
		this.load.image('fire3' , './assets/images/fire3.png');
		this.load.image('smoke' , './assets/images/smoke-puff.png');
		this.load.image('ball' , './assets/images/plasmaball.png');
		this.load.tilemap('level1', './scripts/json/chambre.json', null, Phaser.Tilemap.TILED_JSON);
		this.load.tilemap('level2', './scripts/json/maison.json', null, Phaser.Tilemap.TILED_JSON);
		this.load.tilemap('level3', './scripts/json/base_aerospatiale.json', null, Phaser.Tilemap.TILED_JSON);
		this.load.tilemap('level4', './scripts/json/parc.json', null, Phaser.Tilemap.TILED_JSON);
		this.load.tilemap('level5', './scripts/json/mairie.json', null, Phaser.Tilemap.TILED_JSON);
		this.load.tilemap('level6', './scripts/json/usine.json', null, Phaser.Tilemap.TILED_JSON);
		this.load.tilemap('level7', './scripts/json/donjon1.json', null, Phaser.Tilemap.TILED_JSON);
		this.load.tilemap('level8', './scripts/json/map_2.json', null, Phaser.Tilemap.TILED_JSON);
    	this.load.image('donjon_s1' , './assets/sprites/map/donjon_1.png');
    	this.load.image('donjon_s2' , './assets/sprites/map/donjon_2.png');
    	this.load.image('batiment','./assets/sprites/map/batiment1.png');
    	this.load.image('chambre','./assets/sprites/map/chambre.png');
    	this.load.image('rue','./assets/sprites/map/rue.png');
    	this.load.image('town', './assets/sprites/map/test.png');
    	this.load.image('invisible', './assets/sprites/projectile/invisible.png');
    	this.load.spritesheet('explode', './assets/sprites/projectile/explode.png', 128, 128);
    	this.load.spritesheet('heal', './assets/sprites/projectile/heal.png', 64, 64);
    	this.load.spritesheet('fire', './assets/sprites/projectile/fire.png', 64, 64);
    	this.load.spritesheet('shield', './assets/sprites/projectile/force.png', 192 , 192);
    	this.load.spritesheet('slash', './assets/sprites/projectile/slash.png', 64, 64);
    	this.load.bitmapFont('minecraftia','assets/fonts/font.png','assets/fonts/font.xml');
    	this.game.load.spritesheet('monster1', './assets/sprites/character/blue.png', 16, 21 , 16);
  		this.game.load.spritesheet('monster2', './assets/sprites/character/mecha.png', 64, 64, 16);
  		this.game.load.spritesheet('monster3', './assets/sprites/character/elite.png', 32, 48, 16);
  		this.game.load.spritesheet('boss' , './assets/sprites/character/boss.png', 65, 75 , 16);
  		this.game.load.audio('BS', ['./assets/music/BS.mp3', './assets/music/BS.ogg']);	//On met ogg aussi sinon firefox meurt (parait-il)
    	this.game.load.audio('COB', ['./assets/music/COBCut.mp3', './assets/music/COBCut.ogg']);
    	this.game.load.audio('starcraft', ['./assets/music/StarcraftTerran.mp3', './assets/music/StarcraftTerran.ogg']);
    	this.game.load.audio('nightwish', ['./assets/music/nightwish.mp3', './assets/music/nightwish.ogg']);
    	this.game.load.audio('pirate', ['./assets/music/PirateOTC.mp3', './assets/music/PirateOTC.ogg']);
    	this.game.load.audio('freebird', ['./assets/music/FreeBirdCut.mp3', './assets/music/FreeBirdCut.ogg']);
    	this.game.load.audio('dragonforce', ['./assets/music/dragonforce.mp3', './assets/music/dragonforce.ogg']);
    },

	create: function() {
		this.state.start('MainMenu');
	}
};