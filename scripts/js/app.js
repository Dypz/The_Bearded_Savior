var http = require('http');
var fs = require('fs'),
	util = require('util'),
	Player = require('./Player_class').Player,
	Monster = require('./Monster_class').Monster;


var server = http.createServer(function(req,res){
	fs.readFile('./game.php','utf-8',function(error,content){
		res.writeHead(200,{'Content-Type':'text/html'});
		res.end(content);
	});
});

var io = require('socket.io').listen(server);
var socket;
var	players = new Array(); // tableau des joueurs connectés
var	characters = new Array();
var monsters = new Array();
var id_monstre = 0;

function init(){
	players = [];
	monsters = [];
	server.listen(8080);
	setEventHandlers();
};

var setEventHandlers = function(){
	io.sockets.on('connection',onSocketConnection);
};

function onSocketConnection(client){
	util.log('One player has connected ' + client.id);

	// Gestion des joueurs

	client.on('disconnect',onClientDisconnect);

	client.on('new_player',onNewPlayer);

	client.on('get_connected_players',onGetConnectedPlayers);

	client.on('actualise_position_server',onMovement);

	client.on('animation_server',onAnimation);

	client.on('stop_animation',onStopAnimation);

	client.on('change_map',onChangeMap);

	client.on('set_characteristics',setCharacteristics);

	client.on('send_character',onSaveCharacter);

	client.on('get_characters_account',onRequestCharacter);

	client.on('delete_character',onDeleteCharacter);

	client.on('get_all_characters',onRequestAllCharacter);

	client.on('spell_launched',onSpellLaunched);

	// Gestion des monstres

	client.on('new_monster',onNewMonster);

	client.on('get_monsters_alive',onGetMonstersAlive);

	client.on('actualise_position_monster_server',onMonsterMovement);

	client.on('animation_monster_server',onMonsterAnimation);

	client.on('stop_monster_animation',onStopMonsterAnimation);

	client.on('fire_monster',onFireMovement);

	client.on('kill_monster',onKillMonster);

};

/************************************ Gestion des joueurs ********************************************/

function onNewPlayer(data){
	var new_player = new Player(data.x,data.y,data.job,data.name,data.level,data.exp,data.orientation,data.direction,data.frame,data.strength,data.dexterity,data.intelligence,data.stamina,data.maxHealth,data.currentHealth,data.map);
	new_player.id = this.id;

	this.emit('send_id_player',{id : this.id});

	// on envoie aux joueurs déjà connectés le nouveau joueur
	this.broadcast.emit('add_player',{id: new_player.id, x: new_player.getX(), y : new_player.getY(), job : new_player.getJob(),level : new_player.getLevel(), exp : new_player.getExp(), currentHealth : new_player.getCurrentHealth(), maxHealth : new_player.getMaxHealth(), strength : new_player.getStrength(), dexterity : new_player.getDexterity(), intelligence : new_player.getIntelligence(), stamina : new_player.getStamina(), orientation : new_player.getOrientation(),direction : new_player.getDirection(), frame : new_player.getFrame(), map : new_player.getMap() });
	
	// on ajoute le nouveau joueur au tableau des joueurs connectés
	players.push(new_player);
};

function onGetConnectedPlayers(map)
{
	var connected_players = new Array();
	var player;
	for(var i=0;i<players.length;i++)
	{
		player = {
				id: players[i].id,
				x: players[i].getX(),
				y : players[i].getY(),
				job : players[i].getJob(),
				name : players[i].getName(),
				level : players[i].getLevel(),
				exp : players[i].getExp(),
				currentHealth : players[i].getCurrentHealth(),
				maxHealth : players[i].getMaxHealth(),
				strength : players[i].getStrength(),
				dexterity : players[i].getDexterity(),
				stamina : players[i].getStamina(),
				intelligence : players[i].getIntelligence(),
				orientation : players[i].getOrientation(),
				direction : players[i].getDirection(),
				frame : players[i].getFrame(),
				map : players[i].getMap()
			};
		if(player.map == map)
			connected_players.push(player);
	}
	this.emit('return_connected_players',JSON.stringify(connected_players));
};

function onMovement(data){
	var move_player = findPlayerById(this.id);
	if(!(move_player))
	{
		//util.log('Player not found ' + this.id);
		return;
	}
	if(move_player.getX() != data.x || move_player.getY() != data.y)
	{
		var index = findIndexOfPlayerById(this.id);
		if(index != -1)
		{
			players[index].setX(data.x);
			players[index].setY(data.y);
			this.broadcast.emit('send_new_position',{id: players[index].id, x: players[index].getX(), y : players[index].getY(), map : players[index].getMap() })
		}
		else
		{
			return;
		}
	}			
};

function onAnimation(data){
	var move_player = findPlayerById(this.id);
	if(!(move_player))
	{
		//util.log('Player not found ' + this.id);
		return;
	}
	move_player.setOrientation(data.orientation);
	move_player.setDirection(data.direction);
	this.broadcast.emit('send_new_animation',{id: this.id, direction: data.direction, orientation : data.orientation});
};

function onStopAnimation(data){
	var move_player = findPlayerById(this.id);
	if(!(move_player))
	{
		//util.log('Player not found ' + this.id);
		return;
	}
	move_player.setFrame(data.nb_frame);
	this.broadcast.emit('send_stop_animation',{id: this.id, frame: move_player.getFrame()});
};

function onChangeMap(data){
	var move_player = findPlayerById(this.id);
	if(!(move_player))
	{
		//util.log('Player not found ' + this.id);
		return;
	}
	util.log(this.id + 'change map :'+data.map);
	move_player.setMap(data.map);
	this.broadcast.emit('actualise_players_tab',this.id);
	this.broadcast.emit('add_player',{id: move_player.id, x: move_player.getX(), y : move_player.getY(), job : move_player.getJob(),level : move_player.getLevel(), exp : move_player.getExp(), currentHealth : move_player.getCurrentHealth(), maxHealth : move_player.getMaxHealth(), strength : move_player.getStrength(), dexterity : move_player.getDexterity(), intelligence : move_player.getIntelligence(), stamina : move_player.getStamina(), orientation : move_player.getOrientation(),direction : move_player.getDirection(), frame : move_player.getFrame(), map : move_player.getMap() });
};

function onPlayerDead(id){
	this.broadcast.emit('')
};

function setCharacteristics(data){
	var current_player = findPlayerById(this.id);
	if(!(current_player))
	{
		//util.log('Player not found ' + this.id);
		return;
	}
	switch(data.type)
	{
		case "maxHealth":
			current_player.setMaxHealth(data.value);
			break;
		case "currentHealth":
			current_player.setCurrentHealth(data.value);
			break;
		case "strength":
			current_player.setStrength(data.value);
			break;
		case "dexterity":
			current_player.setDexterity(data.value);
			break;
		case "intelligence":
			current_player.setIntelligence(data.value);
			break;
		case "stamina":
			current_player.setStamina(data.value);
			break;
		case "level":
			current_player.setLevel(data.value);
			break;
		case "exp":
			current_player.setExp(data.value);
			break;
	}
};

function onSaveCharacter(data){
	var character_to_save = JSON.parse(data);
	var to_update = false;
	for(var i=0;i<characters.length;i++)
	{
		if(characters[i].session == character_to_save.session && characters[i].name == character_to_save.name)
		{
			to_update = true;
			break;	
		}
	}
	if(to_update == true)
	{
		characters[i] = character_to_save; // si on a déjà un personnage enregistré avec ce username et ce pseudo alors on met à jour ce personnage	
	}
	else
	{
		characters.push(character_to_save); // sinon on l'ajoute aux tableaux des personnages
	}
};

function onRequestCharacter(username){
	var characters_to_send = new Array();
	for(var i=0;i<characters.length;i++)
	{
		if(characters[i].session == username)
		{
			characters_to_send.push(characters[i]);
		}
	}
	this.emit('return_characters_account',JSON.stringify(characters_to_send));
};

function onDeleteCharacter(data){
	var character_to_delete = JSON.parse(data);
	for(var i=0;i<characters.length;i++)
	{
		if(characters[i].session == character_to_delete.session && characters[i].name == character_to_delete.name)
		{
			characters.splice(i,1);
			break;
		}
	}
	this.emit('delete_succesfull');
};

function onRequestAllCharacter(){
	this.emit('return_all_characters',JSON.stringify(characters));
};

function onSpellLaunched(data){
	this.broadcast.emit('play_spell',{id : data.id,type : data.type});
};

function onClientDisconnect(){
	var remove_player = findPlayerById(this.id);
	if(!(remove_player))
	{
		//util.log('Player not found '+this.id);
		return;
	}
	util.log('One player disconnected '+ this.id);
	// on envoie aux autres joueurs le player à supprimer
	this.broadcast.emit('delete_player',{id: remove_player.id})

	// on supprime le joueur deconnecté du tableau
	players.splice(players.indexOf(remove_player), 1);
};

function findPlayerById(id){
	for(var i=0;i<players.length;i++)
	{
		if(players[i].id == id)
			return players[i];
	}
	return false;
};

function findIndexOfPlayerById(id){
	for(var i=0;i<players.length;i++)
	{
		if(players[i].id == id)
			return i;
	}
	return -1;
};


/************************************************* Gestion des monstres **********************************************/


function onNewMonster(data){
	var new_monster = new Monster(data.x,data.y,data.range,data.hp,data.level,data.exp,data.orientation,data.direction,data.map,data.damage,data.sprite_number);
	new_monster.id = 'monster'+id_monstre;
	util.log('id monstre : '+new_monster.id);
	util.log('map : '+new_monster.getMap());
	util.log('data : '+data.sprite_number);
	util.log('sprite number : '+new_monster.getSpriteNumber());
	
	this.emit('send_id_monster',{id : new_monster.id});
	
	this.broadcast.emit('add_monster',{id: new_monster.id, x: new_monster.getX(), y : new_monster.getY(), range : new_monster.getRange(), hp : new_monster.getHp(), level : new_monster.getLevel(), exp : new_monster.getExp(), orientation : new_monster.getOrientation(),direction : new_monster.getDirection(), map : new_monster.getMap(),damage : new_monster.getDamage(), sprite_number : new_monster.getSpriteNumber() });
	
	monsters.push(new_monster);
	id_monstre++;
};

function onGetMonstersAlive(map)
{
	var monsters_alive = new Array();
	var monster;
	for(var i=0;i<monsters.length;i++)
	{
		monster = {
				id: monsters[i].id,
				x: monsters[i].getX(),
				y : monsters[i].getY(),
				range: monsters[i].getRange(),
				hp : monsters[i].getHp(),
				level : monsters[i].getLevel(),
				exp : monsters[i].getExp(),
				orientation : monsters[i].getOrientation(),
				direction : monsters[i].getDirection(),
				map : monsters[i].getMap(),
				damage : monsters[i].getDamage(),
				sprite_number : monsters[i].getSpriteNumber()
		};
		if(monster.map == map)
			monsters_alive.push(monster);
	}
	this.emit('return_monsters_alive',JSON.stringify(monsters_alive));
};

function onMonsterMovement(data){
	var move_monster = findMonsterById(data.id);
	if(!(move_monster))
	{
		//util.log('Monster not found move' + move_monster.id);
		return;
	}
	if(move_monster.getX() != data.x || move_monster.getY() != data.y)
	{
		var index = findIndexOfMonsterById(data.id);
		if(index != -1)
		{
			monsters[index].setX(data.x);
			monsters[index].setY(data.y);
			this.broadcast.emit('send_new_monster_position',{id: data.id, x: monsters[index].getX(), y : monsters[index].getY()})
		}
		else
		{
			return;
		}
	}			
};

function onMonsterAnimation(data){
	var move_monster = findMonsterById(data.id);
	if(!(move_monster))
	{
		//util.log('Monster not found anim' + move_monster.id);
		return;
	}
	move_monster.setDirection(data.direction);
	this.broadcast.emit('send_new_monster_animation',{id: move_monster.id, direction: move_monster.getDirection() });
};

function onStopMonsterAnimation(id){
	this.broadcast.emit('send_stop_monster_animation',{id: id});
}

function onFireMovement(data){
	this.broadcast.emit('send_fire_monster',{id : data.id, id_player : data.id_player});
}

function onKillMonster(id){
	var remove_monster = findMonsterById(id);
	if(!(remove_monster))
	{
		//util.log('Monster not found delete '+id);
		return;
	}
	util.log('One monster killed '+ id);
	// on envoie aux autres joueurs le monstre à supprimer
	this.broadcast.emit('delete_monster',{id: remove_monster.id})

	// on supprime le monstre tué du tableau du tableau
	monsters.splice(monsters.indexOf(remove_monster), 1);
	id_monstre--;
};

function findMonsterById(id){
	for(var i=0;i<monsters.length;i++)
	{
		if(monsters[i].id == id)
			return monsters[i];
	}
	return false;
};

function findIndexOfMonsterById(id){
	for(var i=0;i<monsters.length;i++)
	{
		if(monsters[i].id == id)
			return i;
	}
	return -1;
};


// Lancement du jeu
init();
