RPG.MainMenu = function(game) {
	RPG.background = null;
};

RPG.MainMenu.prototype = {
	preload: function() {
		this.background = this.add.tileSprite(0, 0, 800, 600, 'MenuBackground');

	},

	create: function() {
		this.createText();
		this.music = this.game.add.audio('BS');
		this.music.play();
	},

	leftAction: function() {
		this.state.start('CharacterSelect');
		this.music.stop();
	},

	rightAction: function() {
		this.state.start('CharacterSelect');
		this.music.stop();
	},

	createText: function() {
		button1 = this.game.add.button( 450, 350, 'bouton1up', this.rightAction,this);
		button2 = this.game.add.button(50,350,'bouton2up',this.leftAction,this);
	}
};	