// ability class
function Ability(game) {
	this.game = game;
	// damage type: either Physical or Magical
	this.damageType = "Magical";
	this.name = "";
	this.baseMinDamage = 0;
	this.baseMaxDamage = 0;
	// range in pixels (works as a circle with the character as center)
	this.range = 100;
	// animation of ability
	this.sprite = null;
	// icon of ability
	this.icon = null;
	this.cooldown = 0;
}

Ability.prototype = {
	getDamageType: function() {
		return this.damageType;
	},

	setDamageType: function(dmgType) {
		if (dmgType == "Physical" || dmgType == "Magical") {
			this.damageType = dmgType;
		}
	},

	getName: function() {
		return this.name;
	},

	setName: function(str) {
		if (typeof str == "string") {
			this.name = str;
		}
	},

	getBaseMinDamage: function() {
		return this.baseMinDamage;
	},

	getBaseMaxDamage: function() {
		return this.baseMaxDamage;
	},

	setBaseMinDamage: function(dmg) {
		if (dmg >= 0) {
			this.baseMinDamage = dmg;
		}
	},

	setBaseMaxDamage: function(dmg) {
		if (dmg >= 0) {
			this.baseMaxDamage = dmg;
		}
	},

	getRange: function() {
		return this.range;
	},

	setRange: function (r) {
		if (r > 0) {
			this.range = r;
		}
	},

	isAvailable: function() {
		return this.available;
	},

	setAvaibility: function(b) {
		if (typeof b == "boolean") {
			this.available = b;
		}
	}
};