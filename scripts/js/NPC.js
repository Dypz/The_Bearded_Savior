/*global Game*/
/*global tileSize*/
/*global dialogue*/


var Npc = function(game) {
  this.game = game;
  this.sprite = null;
  this.alive = true;
  this.tilex = 0;
  this.tiley = 0;
  this.orientation = 1;
  this.hidden = true; //hide dialogue box on creation
  this.sprite = null;
  this.line = '';
  //this.text = '';
  this.typing = false;
  this.speaker = null; //the sprite/npc currently speaking
  this.index = 0;
  this.line = '';
  this.isSpeaking = false;
  this.textOk = false;
  this.actualText = "";
  this.lineNumber = 1;
  this.dialogWait = false;
  this.JustFinishToTalk=false;
};

Npc.prototype = {
  preload: function( sprite ) {
  	//On charghe le sprite du joueur avec la taille x*y et le nombre de frame, ici 16
  	this.game.load.image('textbox','assets/images/textboxF.png',64,64);
  },
  //X position ; y position ; sa portée de tir (mettre 0 pour corps à corps ?) ; et son Id
  //La range est évalué au carré par exemple 42 donne environ 32 pixel d'écartzaA
  create: function( x , y , Id , dialog , name) {
    //Création de notre personnage
    
    this.sprite = this.game.add.sprite(x,y,'npc'+Id);
	this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
    this.sprite.anchor.setTo(0.5,0.5);

  	this.sprite.animations.add('down', [0, 1, 2, 3], 6, true , true);	//2
	this.sprite.animations.add('left', [4, 5, 6, 7], 6, true , true);	//3
    this.sprite.animations.add('right', [8, 9, 10, 11], 6, true , true);	//4
	this.sprite.animations.add('up', [12, 13, 14, ], 6, true , true);	//1

	//Collision sur les bords
	this.sprite.body.collideWorldBounds = true;
	this.sprite.body.immovable = true;
	//Set Frames for facing
	this.LEFT = 4;
	this.RIGHT = 8;
	this.UP = 12;
	this.DOWN = 0;

	this.content = dialog;	//L'idée est de passer en argument un tableau de string ; avec chaque case du tableau un réplique et faire varier l'index dans le jeu.
	this.currentScene = 0;	//On sauvegarde la réplique du bonhomme

	this.name = name;

  },

	update: function(player , game) {

    	var marge = 60; // = Distance de dialogue
    	//Vérifions si notre player est proche du NPC et si il appuie sur la touche du dialogue
    	if (this.dialogWait == true)
    	{	
			if (this.isSpeaking == true && player.talk.isDown)
			{
				this.delay(500);
				this.newPage();
				//this.game.time.events.add(50, this.newPage , this);
			}
			else if (this.isSpeaking == false && player.talk.isDown)
			{
				this.delay(500);
				this.endDialog( player );
				player.stage++;
				this.delay(500);
			}
    	}
    	else if (this.isSpeaking == true && this.dialogWait == false)
    	{
    		this.game.time.events.add(5, this.writeLetter , this);
    	}
    	else if (this.game.physics.arcade.distanceBetween(this.sprite , player.sprite) < 50 && player.talk.isDown) 
    	{
    		if (this.JustFinishToTalk==true){
    			this.delay(800);
    			this.JustFinishToTalk=false;
    		} 
    		//Mettons le face au joueur en vérifiant l'orientation du joueur
    		if (this.faceToPlayer( player ) && this.isSpeaking == 0)
    		{
    			//this.show(this.dialog[this.currentScene] , game);
    			//Maintenant lançons le dialogue !
    			player.isTalking = true;
    			this.createTextbox();
    		}
    	}
  } ,
  	//Cette fonction vérifie le positionement du joueur par rapport au sprite et change alors l'orientation du NPC ;
  	//Elle renvoie true si le joueur et le NPC peuvent dialoguer ; et false dans le cas contraire.
	faceToPlayer : function (player)
	{
		var marge = 60;
		var result = false;
		if (player.sprite.x <= this.sprite.x + marge && player.sprite.x > this.sprite.x - marge)
		{
			if (player.sprite.y < this.sprite.y && player.orientation == 3)
			{
				this.sprite.frame = this.UP;
				result = true;
			}
			else if (player.orientation == 1)
			{
				this.sprite.frame = this.DOWN;
				result = true;
			}
		}
		else if (player.sprite.y <= this.sprite.y + marge && player.sprite.y > this.sprite.y - marge)
		{
			if (player.sprite.x < this.sprite.x && player.orientation == 4)
			{
				this.sprite.frame = this.LEFT;
				result = true;
			}
			else if (player.orientation == 2)
			{
				this.sprite.frame = this.RIGHT;
				result = true;
			}
		}
		return result;
	} ,

	createTextbox : function ()
	{
		this.textbox = this.game.add.sprite(this.game.camera.x + 15 , this.game.camera.y+325 ,'textbox');
		this.textbox.z = 0;
		this.username = this.game.add.text(60, 350 , '' , { font: "30px Arial", fill: "#ffffff", align: "center" });
		this.username.setText(this.name);
		this.username.fixedToCamera = true;
		this.textL1 = this.game.add.text(40, 425, '' , { font: "30px Arial", fill: "#ffffff", align: "center" });
		this.textL1.fixedToCamera = true;
		this.textL2 = this.game.add.text(40, 475, '' , { font: "30px Arial", fill: "#ffffff", align: "center" });
		this.textL2.fixedToCamera = true;
		this.textL3 = this.game.add.text(40, 525, '' , { font: "30px Arial", fill: "#ffffff", align: "center" });
		this.textL3.fixedToCamera = true;
    	//this.text = game.add.text(32, 380, '', { font: "30pt Courier", fill: "#19cb65", stroke: "#119f4e", strokeThickness: 2 });
    	//this.textbox.z = 1;
    	this.isSpeaking = true;
    	return true;
	} ,

	write : function ()
	{
		while(this.textOk == false)
		{
			this.game.time.events.add(50, this.writeLetter , this);
		}
		
	} , 

	writeLetter : function()
	{
		//this.actualText += this.content[this.currentScene][this.index];
		this.delay(10);
		if (this.index >= this.content[this.currentScene].length)
		{
			this.index = 0;
			this.lineNumber = 1;
			this.isSpeaking = false;
			this.dialogWait = true;
		}
		else if (this.lineNumber == 1)
		{
			this.actualText += this.content[this.currentScene][this.index];
			this.textL1.setText(this.actualText);
			this.index++;
			if (this.content[this.currentScene][this.index] == '*')
			{
				this.index++;
				this.lineNumber = 2;
				this.actualText = "";
			}
		}
		else if (this.lineNumber == 2)
		{
			this.actualText += this.content[this.currentScene][this.index];
			this.textL2.setText(this.actualText);
			this.index++;
			if (this.content[this.currentScene][this.index] == '*')
			{
				this.index++;
				this.lineNumber = 3;
				this.actualText = "";
			}
		}
		else if (this.lineNumber == 3)
		{
			this.actualText += this.content[this.currentScene][this.index];
			this.textL3.setText(this.actualText);
			this.index++;
			if (this.content[this.currentScene][this.index] == '*')
			{
				this.dialogWait = true;;
			}
			
		}
	} , 

	newPage : function()
	{
		this.index++;
		this.actualText = "";
		this.lineNumber = 1;
		this.dialogWait = false;
		this.textL1.setText("");
		this.textL2.setText("");
		this.textL3.setText("");
	} ,

	endDialog : function( player )
	{
		this.index = 0;
		this.textbox.kill();
		this.actualText = "";
		this.lineNumber = 1;
		this.dialogWait = false;
		this.username.setText("");
		this.textL1.setText("");
		this.textL2.setText("");
		this.textL3.setText("");
		player.isTalking = false;
		this.JustFinishToTalk=true;

	} ,
	//En milliseconde
	delay : function ( time )
	{
		var date = new Date();
		var curDate = null;
		do { curDate = new Date(); }
		while(curDate-date < time );
	} 
	/*,

	show : function( content , Game ) {
    if (this.typing) {
     return;
    }
    this.createTextbox();
    this.speaker = this.npcName;
    this.content = content;
    this.typing = true;
    this.hidden = false;

    /* Position sprite below the current screen and make it visible
    //this.textbox.x = Game.camera.x*Game.w;
    //this.textbox.y = Game.camera.y*Game.h+11*64;
    this.textbox.x = 55;
    this.textbox.y = 55;

    this.text.x = Game.camera.x*Game.w+30;
    this.text.y = Game.camera.y*Game.h+7*64+30;
 
    this.textbox.z = 1;


    /* Slide Up the Dialogue Panel 
    var t = this.game.add.tween(this.textbox).to({x: Game.camera.x*Game.w, y: Game.camera.y*Game.h+7*64}, 250);
    t.start();
    t.onComplete.add(function() {
      this.nextLine();
    }, this);
  },

  hide: function( Game ) {
    if (this.hidden === true) {
      return;
    }

    /* Reset to defaults
    this.text.setText('');
    this.line = '';
    this.index = 0;

    /* Slide Down the Dialogue Panel and make it invisible 
    var t = this.game.add.tween(this.textbox).to({x: Game.camera.x*Game.w, y: Game.camera.y*Game.h+11*64}, 250);
    t.start();
    t.onComplete.add(function(){
      this.hidden = true;
      this.textbox.z = 0;
    }, this);
  } /*,

  nextLine: function() {
    this.index++;

    if (this.index < this.content.length)
    {
        this.line = '';
        this.game.time.events.repeat(20, this.content[this.index].length + 1, this.updateLine, this);
    }else {
      this.typing = false;
      this.speaker = null;
      this.text.setText(this.line+' *');
    }
  },

  updateLine: function() {
  	console.log(this.line);
  	/*
      if (this.line.length < this.content[this.index].length)
      {
          this.line = this.content[this.index].substr(0, this.line.length + 1);
          this.text.setText(this.line);
      }
      else
      {
          //  Wait 1 seconds then start a new line
          this.game.time.events.add(Phaser.Timer.SECOND, this.nextLine, this);
      }
  }*/
};