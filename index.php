<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link href="css/style.css" rel='stylesheet' type='text/css' />
		<script type="text/javascript" src="./scripts/js/checkIndex.js"></script>
		<title>Let's play</title>
		<script type="text/javascript">
			sessionStorage.removeItem("username");
		</script>
	</head>
	<body onload="init()">
		<div id="message"></div>
		<?php 
			session_start();
			$message = '';
			$type = '';
			if(isset($_SESSION['informations']) && isset($_SESSION['type']))
			{
				$message = $_SESSION['informations'];
				$type = $_SESSION['type'];
				unset($_SESSION['informations']);
				unset($_SESSION['type']);
			}

			if(isset($_COOKIE['username']) && isset($_COOKIE['password'])) // si des cookies sont existants
			{
				// on va vérifier que ces cookies sont valables avant de rediriger directement l'utilisateur vers le jeu
				require './includes_php/connect.php';
		
				$back = $db->query("SELECT username, password FROM users WHERE username = \"".$_COOKIE['username']."\"");	
				$row = $back->fetch_array();
				if($row['username'] != '')
				{
					if($row['password'] == $_COOKIE['password'])
					{
						// dans ce cas les cookies sont valides donc on renvoie l'utilisateur vers le jeu
						$_SESSION['username'] = $_COOKIE['username'];
						$_SESSION['password'] = $_COOKIE['password'];
						header('location: game.php');
					}
					else
					{
						setcookie('username','',time()-3600); // on détruit les cookies si l'un d'entre eux n'est pas valable
						setcookie('password','',time()-3600);
					}
				}
				else
				{
					setcookie('username','',time()-3600); // on détruit les cookies si l'un d'entre eux n'est pas valable
					setcookie('password','',time()-3600);
				}
			}
		?>
		<script type="text/javascript">
				var message = '<?php echo $message ?>';
				var type = '<?php echo $type ?>';
				if(message != '')
				{
					document.getElementById('message').style.visibility = 'visible';
					if(type == 'error')
					{
						document.getElementById('message').innerHTML = "<img src='./assets/images/errorimg.png' class='error_img' /> "+message;
					}
					else
					{
						document.getElementById('message').innerHTML = "<img src='./assets/images/valid.png' class='valid_img' /> "+message;
					}
					
				}
		</script>
		<div class="login-form">
			<h1>Sign In</h1>
			<h2><a href="./includes_php/register.php">Create Account</a></h2>
		
			<form action="./includes_php/login.php" method="post" onsubmit="return checkFormLogin()" >
				<li>
					<input type="text" class="text" id="username" name="username" placeholder="Username"  onblur="checkInput(this);" /><a href="#" class=" icon user"></a>
				</li>
				<li>
					<input type="password" id="password" name="password" placeholder="Password" onblur="checkInput(this);" /><a href="#" class=" icon lock"></a>
				</li>
				<input type="submit" value="Sign in" /><a href="#" class=" icon arrow"></a> 
			</form>
		</div>
	</body>
</html>