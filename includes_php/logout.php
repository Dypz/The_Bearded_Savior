<?php
	// si l'utilisateur se déconnecte, on détruit tous les cookies et les sessions
	session_start();
	session_unset();
	session_destroy();
	foreach($_COOKIE as $cle => $element)
	{
		setcookie($cle, '', time()-3600);
	}
	header('location : ../index.php');
?>