<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<script type="text/javascript" src="../scripts/js/checkRegister.js"></script>
		<link href="../css/style.css" rel='stylesheet' type='text/css' />
		<title>Register page</title>
	</head>
	<body onload="init()">
		<div id="message"></div>
		<?php 
			session_start();
			$message = '';
			$type = '';
			if(isset($_SESSION['informations']) && isset($_SESSION['type']))
			{
				$message = $_SESSION['informations'];
				$type= $_SESSION['type'];
				unset($_SESSION['informations']);
				unset($_SESSION['type']);
			}
		?>
		<script type="text/javascript">
				var message = '<?php echo $message ?>';
				var type = '<?php echo $type ?>';
				if(message != '')
				{
					document.getElementById('message').style.visibility = 'visible';
					if(type == 'error')
					{
						document.getElementById('message').innerHTML = "<img src='./assets/images/errorimg.png' class='error_img' /> "+message;
					}
					else
					{
						document.getElementById('message').innerHTML = "<img src='./assets/images/valid.png' class='valid_img' /> "+message;
					}
					
				}
		</script>
		<div class="login-form">
			<h1>Creating your account</h1>
			<h2><a href="../index.php">Sign in</a></h2>
		
			<form action="treatment_register.php" method="post" onsubmit="return checkFormRegister()" >
				<li>
					<input type="text" class="text" id="username" name="username" placeholder="Username"  onblur="checkInput(this);" /><a href="#" class=" icon user"></a>
				</li>
				<li>
					<input type="password" id="password" name="password" placeholder="Password" onblur="checkInput(this);" /><a href="#" class=" icon lock"></a>
				</li>
				<li>
					<input type="text" id="mail" name="mail" placeholder="mail@address" onblur="checkMail(this);" /><a href="#" class=" icon mail"></a>
				</li>
				<input type="submit" value="Create" /><a href="#" class=" icon arrow"></a> 
			</form>
		</div>
	</body>
</html>